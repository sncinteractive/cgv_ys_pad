package com.cgv.ys.manager.pad.cgv_ys.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by eklee on 2017. 6. 19..
 */

public class LiveScheduleDetailModel {
    @SerializedName("YSH_Index")
    private int mIndex;

    @SerializedName("YSH_Name")
    private String mName;

    @SerializedName("Scenes")
    private List<SceneModel> mSceneModelList;

    public LiveScheduleDetailModel() {
    }

    public LiveScheduleDetailModel(int index, String name, List<SceneModel> sceneModelList) {
        mIndex = index;
        mName = name;
        mSceneModelList = sceneModelList;
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public List<SceneModel> getSceneModelList() {
        return mSceneModelList;
    }

    public void setSceneModelList(List<SceneModel> sceneModelList) {
        mSceneModelList = sceneModelList;
    }
}
