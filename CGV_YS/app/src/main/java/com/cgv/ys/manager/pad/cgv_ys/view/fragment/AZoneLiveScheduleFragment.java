package com.cgv.ys.manager.pad.cgv_ys.view.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cgv.ys.manager.pad.cgv_ys.R;
import com.cgv.ys.manager.pad.cgv_ys.common.AlertDialogButtonCallback;
import com.cgv.ys.manager.pad.cgv_ys.common.DebugLog;
import com.cgv.ys.manager.pad.cgv_ys.common.Defines;
import com.cgv.ys.manager.pad.cgv_ys.common.Enums;
import com.cgv.ys.manager.pad.cgv_ys.common.Util;
import com.cgv.ys.manager.pad.cgv_ys.model.LayerContent;
import com.cgv.ys.manager.pad.cgv_ys.model.LiveScheduleDetailModel;
import com.cgv.ys.manager.pad.cgv_ys.model.LiveScheduleListItemModel;
import com.cgv.ys.manager.pad.cgv_ys.model.SceneLayerModel;
import com.cgv.ys.manager.pad.cgv_ys.model.SceneModel;
import com.cgv.ys.manager.pad.cgv_ys.network.NetClient;
import com.cgv.ys.manager.pad.cgv_ys.network.NetResponse;
import com.cgv.ys.manager.pad.cgv_ys.network.NetResponseCallback;
import com.cgv.ys.manager.pad.cgv_ys.view.ViewHolder.VideoLayerViewHolder;
import com.cgv.ys.manager.pad.cgv_ys.view.adapter.LiveScheduleListAdapter;
import com.cgv.ys.manager.pad.cgv_ys.view.adapter.SceneListAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

public class AZoneLiveScheduleFragment extends FragmentBase implements View.OnClickListener {
    private static final String TAG = "AZoneLiveScheduleFragment";

    private static final int MAX_VIDEO_LAYER_COUNT = 3;

    private View mViewMyself;

    public interface OnFragmentInteractionListener {
        void onRequestMqttLiveSchedule(String message);
    }

    private OnFragmentInteractionListener mOnFragmentInteractionListener;

    private List<LiveScheduleListItemModel> mLiveScheduleListItemModelList;
    private RecyclerView mScheduleListRecyclerView;
    private LiveScheduleListItemModel mCurSelectedScheduleListItemModel;

    private View mPreviewLayout;
    private TextView mPreviewLiveScheduleName;
    private TextView mPreviewSceneIndexTextView;
    private TextView mPreviewSceneNameTextView;
    private ImageView mPreviewAudioIconImageView;
    private Button mLiveScheduleEndButton;
    private List<VideoLayerViewHolder> mPreviewVideoLayerViewHolderList;

    private List<SceneModel> mSceneModelList;
    private RecyclerView mSceneListRecyclerView;
    private SceneModel mCurPreviewedSceneModel;

    private LiveScheduleListItemModel mAppliedScheduleListItemModel;
    private SceneModel mAppliedSceneModel;

    public AZoneLiveScheduleFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            mViewMyself = inflater.inflate(R.layout.fragment_live_schedule, container, false);

            super.initializeView(mViewMyself);

            setSelectedTabButton(Enums.TabType.LIVE_SCHEDULE);

            mLiveScheduleListItemModelList = new ArrayList<>();
            mScheduleListRecyclerView = (RecyclerView) mViewMyself.findViewById(R.id.rv_live_schedule_list);
            mScheduleListRecyclerView.setAdapter(new LiveScheduleListAdapter(mLiveScheduleListItemModelList, new ScheduleListItemInteractionListener()));

            mPreviewLayout = mViewMyself.findViewById(R.id.preview_layout);

            mPreviewLiveScheduleName = (TextView) mPreviewLayout.findViewById(R.id.tv_preview_live_schedule_name);
            mPreviewSceneIndexTextView = (TextView) mPreviewLayout.findViewById(R.id.tv_scene_index);
            mPreviewSceneNameTextView = (TextView) mPreviewLayout.findViewById(R.id.tv_scene_name);
            mPreviewAudioIconImageView = (ImageView) mPreviewLayout.findViewById(R.id.iv_audio_icon);

            mPreviewVideoLayerViewHolderList = new ArrayList<>(MAX_VIDEO_LAYER_COUNT);
            mPreviewVideoLayerViewHolderList.add(VideoLayerViewHolder.createInstance(mPreviewLayout.findViewById(R.id.rl_video_layer1)));
            mPreviewVideoLayerViewHolderList.add(VideoLayerViewHolder.createInstance(mPreviewLayout.findViewById(R.id.rl_video_layer2)));
            mPreviewVideoLayerViewHolderList.add(VideoLayerViewHolder.createInstance(mPreviewLayout.findViewById(R.id.rl_video_layer3)));

            mSceneModelList = new ArrayList<>();
            mSceneListRecyclerView = (RecyclerView) mPreviewLayout.findViewById(R.id.rv_scene_list);
            mSceneListRecyclerView.setAdapter(new SceneListAdapter(mSceneModelList, new SceneListItemInteractionListener()));

            Button closePreviewButton = (Button) mPreviewLayout.findViewById(R.id.btn_preview_close);
            closePreviewButton.setOnClickListener(this);

            mLiveScheduleEndButton = (Button) mPreviewLayout.findViewById(R.id.btn_live_schedule_end);
            mLiveScheduleEndButton.setOnClickListener(this);

            hidePreview();

            requestLiveScheduleList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mViewMyself;
    }

    private void requestLiveScheduleList() {
        try {
            showMainActiviyLoadingProgressBar();

            RequestParams requestParams = new RequestParams();
            requestParams.put("ScheduleType", "AL");
            requestParams.put("CurrentPage", 1);
            requestParams.put("PerPageNum", 10000);

            NetClient.post(getContext(), Defines.URL_GET_LIVE_SCHEDULE_LIST, requestParams, new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(String jsonResponse) {
                    try {
                        hideMainAcitivyLoadingProgressBar();

                        boolean isValid = false;
                        if (!Util.isStringNullOrEmpty(jsonResponse)) {

                            mLiveScheduleListItemModelList.clear();

                            SharedPreferences prefs = Util.getSharedPreferences(getContext());
                            int appliedScheduleIndex = prefs.getInt(Defines.PREFERENCES_KEY_LIVE_SCHEDULE_INDEX, -1);

                            Gson gson = new Gson();
                            try {
                                List<LiveScheduleListItemModel> newList = gson.fromJson(jsonResponse, new TypeToken<List<LiveScheduleListItemModel>>() {
                                }.getType());
                                if (newList != null) {
                                    for (LiveScheduleListItemModel model : newList) {
                                        mLiveScheduleListItemModelList.add(model);

                                        if (appliedScheduleIndex != -1 && appliedScheduleIndex == model.getIndex()) {
                                            mAppliedScheduleListItemModel = model;
                                            mAppliedScheduleListItemModel.setApplied(true);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (appliedScheduleIndex != -1 && mAppliedScheduleListItemModel == null) {
                                prefs.edit().putInt(Defines.PREFERENCES_KEY_LIVE_SCHEDULE_INDEX, -1).apply();
                                prefs.edit().putInt(Defines.PREFERENCES_KEY_LIVE_SCHEDULE_SCENE_INDEX, -1).apply();
                            }

                            mScheduleListRecyclerView.getAdapter().notifyDataSetChanged();

                            isValid = true;
                        }

                        if (!isValid) {
                            Util.showAlertDialog(getContext(),
                                getString(R.string.dialog_title),
                                getString(R.string.dialog_message_get_schedule_list_fail),
                                getString(R.string.retry),
                                getString(R.string.finish),
                                new AlertDialogButtonCallback() {
                                    @Override
                                    public void onClickButton(boolean isPositiveClick) {
                                        if (isPositiveClick) {
                                            requestLiveScheduleList();
                                        } else {
                                            getActivity().finish();
                                        }
                                    }
                                });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
            hideMainAcitivyLoadingProgressBar();
        }
    }

    private void requestLiveScheduleDetail() {
        try {
            showMainActiviyLoadingProgressBar();

            RequestParams requestParams = new RequestParams();
            requestParams.put("YSH_INDEX", mCurSelectedScheduleListItemModel.getIndex());

            NetClient.post(getContext(), Defines.URL_GET_LIVE_SCHEDULE_DETAIL, requestParams, new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(String jsonResponse) {
                    try {
                        hideMainAcitivyLoadingProgressBar();

                        if (!Util.isStringNullOrEmpty(jsonResponse)) {
                            mSceneModelList.clear();

                            SharedPreferences prefs = Util.getSharedPreferences(getContext());
                            int appliedSceneIndex = prefs.getInt(Defines.PREFERENCES_KEY_LIVE_SCHEDULE_SCENE_INDEX, -1);
                            boolean foundApplied = false;

                            Gson gson = new Gson();
                            try {
                                LiveScheduleDetailModel liveScheduleDetailModel = gson.fromJson(jsonResponse, LiveScheduleDetailModel.class);
                                if (liveScheduleDetailModel != null) {
                                    List<SceneModel> newList = liveScheduleDetailModel.getSceneModelList();
                                    if (newList != null) {
                                        for (SceneModel model : newList) {
                                            mSceneModelList.add(model);

                                            if (appliedSceneIndex != -1 && appliedSceneIndex == model.getIndex()) {
                                                model.setApplied(true);
                                                foundApplied = true;
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (foundApplied == false) {
                                prefs.edit().putInt(Defines.PREFERENCES_KEY_LIVE_SCHEDULE_SCENE_INDEX, -1).apply();
                            }

                            if (!mSceneModelList.isEmpty()) {
                                showPreview();
                            } else {
                                Util.showAlertDialog(getContext(),
                                    getString(R.string.dialog_title),
                                    getString(R.string.dialog_message_get_schedule_detail_fail),
                                    getString(R.string.confirm),
                                    getString(R.string.retry),
                                    new AlertDialogButtonCallback() {
                                        @Override
                                        public void onClickButton(boolean isPositiveClick) {
                                            if (!isPositiveClick) {
                                                requestLiveScheduleList();
                                            }
                                        }
                                    });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
            hideMainAcitivyLoadingProgressBar();
        }
    }

    private class ScheduleListItemInteractionListener implements LiveScheduleListAdapter.ItemInteractionListener {
        @Override
        public void onClick(int position) {
            try {
                mCurSelectedScheduleListItemModel = mLiveScheduleListItemModelList.get(position);
                requestLiveScheduleDetail();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class SceneListItemInteractionListener implements SceneListAdapter.ItemInteractionListener {
        @Override
        public void onApply(final int position) {
            try {
                if (mAppliedScheduleListItemModel == null) {
                    String startMessage = "LiveSchedule|Start," + mCurSelectedScheduleListItemModel.getIndex();
                    mOnFragmentInteractionListener.onRequestMqttLiveSchedule(startMessage);
                } else {
                    mAppliedScheduleListItemModel.setApplied(false);
                }

                mAppliedScheduleListItemModel = mCurSelectedScheduleListItemModel;
                mAppliedScheduleListItemModel.setApplied(true);

                mScheduleListRecyclerView.getAdapter().notifyDataSetChanged();

                if (mAppliedSceneModel != null) {
                    mAppliedSceneModel.setApplied(false);
                }

                mAppliedSceneModel = mSceneModelList.get(position);
                mAppliedSceneModel.setApplied(true);

                mSceneListRecyclerView.getAdapter().notifyDataSetChanged();

                String sceneMessage = "LiveSchedule|CScene," + mAppliedScheduleListItemModel.getIndex() + "," + mAppliedSceneModel.getIndex();
                mOnFragmentInteractionListener.onRequestMqttLiveSchedule(sceneMessage);

                SharedPreferences prefs = Util.getSharedPreferences(getContext());
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt(Defines.PREFERENCES_KEY_LIVE_SCHEDULE_INDEX, mAppliedScheduleListItemModel.getIndex());
                editor.putInt(Defines.PREFERENCES_KEY_LIVE_SCHEDULE_SCENE_INDEX, mAppliedSceneModel.getIndex());
                editor.apply();

                onPreview(position);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onPreview(int position) {
            if (mCurPreviewedSceneModel != null) {
                mCurPreviewedSceneModel.setPreviewed(false);
            }

            mCurPreviewedSceneModel = mSceneModelList.get(position);
            mCurPreviewedSceneModel.setPreviewed(true);

            mSceneListRecyclerView.getAdapter().notifyDataSetChanged();

            applySceneLayerPreview(position);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mOnFragmentInteractionListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mOnFragmentInteractionListener = null;
    }

    @Override
    public void onClick(View v) {
        try {
            super.onClick(v);

            switch (v.getId()) {
                case R.id.btn_live_schedule_end: {
                    terminateAppliedLiveSchedule();
                }
                break;
                case R.id.btn_preview_close: {
                    hidePreview();
                }
                break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void terminateAppliedLiveSchedule() {
        if (mAppliedScheduleListItemModel != null) {
            mAppliedScheduleListItemModel.setApplied(false);

            mScheduleListRecyclerView.getAdapter().notifyDataSetChanged();

            mOnFragmentInteractionListener.onRequestMqttLiveSchedule("LiveSchedule|End," + mAppliedScheduleListItemModel.getIndex());

            SharedPreferences prefs = Util.getSharedPreferences(getContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(Defines.PREFERENCES_KEY_LIVE_SCHEDULE_INDEX, -1);
            editor.putInt(Defines.PREFERENCES_KEY_LIVE_SCHEDULE_SCENE_INDEX, -1);
            editor.apply();
        }
        mAppliedScheduleListItemModel = null;
        mAppliedSceneModel = null;

        hidePreview();
    }

    private void showPreview() {
        try {
            if (mSceneModelList.isEmpty()) {
                Util.showAlertDialog(getContext(),
                    getString(R.string.dialog_title),
                    getString(R.string.dialog_message_no_scene_list),
                    getString(R.string.confirm),
                    null,
                    null);
                return;
            }

            showBackground();

            mPreviewLayout.setVisibility(View.VISIBLE);

            mPreviewLiveScheduleName.setText(getString(R.string.live_schedule_preview_name_format, mCurSelectedScheduleListItemModel.getName()));

            mCurPreviewedSceneModel = mSceneModelList.get(0);
            mCurPreviewedSceneModel.setPreviewed(true);

            mSceneListRecyclerView.getAdapter().notifyDataSetChanged();

            applySceneLayerPreview(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void applySceneLayerPreview(int position) {
        try {
            mPreviewSceneIndexTextView.setText(Integer.toString(position + 1));
            mPreviewSceneNameTextView.setText(mCurPreviewedSceneModel.getName());

            SharedPreferences prefs = Util.getSharedPreferences(getContext());
            int scheduleIndex = prefs.getInt(Defines.PREFERENCES_KEY_LIVE_SCHEDULE_INDEX, -1);
            int sceneIndex = prefs.getInt(Defines.PREFERENCES_KEY_LIVE_SCHEDULE_SCENE_INDEX, -1);
            if (scheduleIndex == mCurSelectedScheduleListItemModel.getIndex() &&
                sceneIndex == mCurPreviewedSceneModel.getIndex()) {
                mLiveScheduleEndButton.setVisibility(View.VISIBLE);
            } else {
                mLiveScheduleEndButton.setVisibility(View.GONE);
            }

            List<SceneLayerModel> sceneLayerModelList = mCurPreviewedSceneModel.getSceneLayerModelList();
            if (sceneLayerModelList == null || sceneLayerModelList.isEmpty()) {
                Util.showAlertDialog(getContext(),
                    getString(R.string.dialog_title),
                    getString(R.string.dialog_message_no_layer_content),
                    getString(R.string.confirm),
                    null,
                    null);

            } else {
                int size = sceneLayerModelList.size();
                for (int i = 0; i < size; i++) {
                    SceneLayerModel sceneLayerModel = sceneLayerModelList.get(i);
                    if (sceneLayerModel == null) {
                        continue;
                    }

                    LayerContent layerContent = sceneLayerModel.getLayerContent();
                    if (layerContent == null) {
                        continue;
                    }

                    switch (sceneLayerModel.getType()) {
                        case "VD": {
                            int viewHolderIndex = sceneLayerModel.getCol() / 2;
                            if (viewHolderIndex >= 0 && viewHolderIndex < mPreviewVideoLayerViewHolderList.size()) {
                                VideoLayerViewHolder viewHolder = mPreviewVideoLayerViewHolderList.get(viewHolderIndex);
                                viewHolder.bindData(getVideoLayerName(layerContent), getLayerIconResID(layerContent.getLayerContentType()));
                            } else {
                                DebugLog.e(TAG, "scene video layer index is out of range. index = " + viewHolderIndex);
                            }
                        }
                        break;
                        case "AD": {
                            mPreviewAudioIconImageView.setImageResource(getLayerIconResID(layerContent.getLayerContentType()));
                        }
                        break;
                        default:
                            break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hidePreview() {
        mPreviewLayout.setVisibility(View.GONE);
        hideBackground();
    }

    private int getLayerIconResID(String type) {
        switch (type) {
            case "CM":
                return R.drawable.icon_cam_large;
            case "PL":
                return R.drawable.icon_poster;
            case "MC":
                return R.drawable.icon_mic_blue;
            case "MT":
                return R.drawable.icon_mute_blue;
            case "SP":
                return R.drawable.icon_speaker_blue;
            default:
                return -1;
        }
    }

    private String getVideoLayerName(LayerContent layerContent) {
        if (layerContent.getLayerContentType().equals("PL")) {
            return layerContent.getRefName();
        } else {
            return layerContent.getInputName();
        }
    }
}