package com.cgv.ys.manager.pad.cgv_ys.common;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by eklee on 2017. 6. 24..
 */

public class DebugLog {

    private static boolean sEnable = false;

    private static File sLogFile = null;

    public static void setEnable(boolean enable) {
        DebugLog.sEnable = enable;
    }

    public static boolean isEnable() {
        return sEnable;
    }

    public static final void e(String tag, String message) {
        e(tag, message, true);
    }

    public static final void e(String tag, String message, boolean logToFile) {
        e(tag, message, false, logToFile);
    }

    public static final void e(String tag, String message, boolean showStackTrace, boolean logToFile) {
        if (sEnable) {
            if (showStackTrace) {
                Log.e(tag, generateStackTraceMessage(message));
            } else {
                Log.e(tag, message);
            }

            if (logToFile) {
                logToFile(tag + " E " + message);
            }
        }
    }

    public static final void w(String tag, String message) {
        w(tag, message, true);
    }

    public static final void w(String tag, String message, boolean logToFile) {
        w(tag, message, false, logToFile);
    }

    public static final void w(String tag, String message, boolean showStackTrace, boolean logToFile) {
        if (sEnable) {
            if (showStackTrace) {
                Log.w(tag, generateStackTraceMessage(message));
            } else {
                Log.w(tag, message);
            }

            if (logToFile) {
                logToFile(tag + " W " + message);
            }
        }
    }

    public static final void d(String tag, String message) {
        d(tag, message, true);
    }

    public static final void d(String tag, String message, boolean logToFile) {
        d(tag, message, false, logToFile);
    }

    public static final void d(String tag, String message, boolean showStackTrace, boolean logToFile) {
        if (sEnable) {
            if (showStackTrace) {
                Log.d(tag, generateStackTraceMessage(message));
            } else {
                Log.d(tag, message);
            }

            if (logToFile) {
                logToFile(tag + " D " + message);
            }
        }
    }

    public static final void i(String tag, String message) {
        i(tag, message, true);
    }

    public static final void i(String tag, String message, boolean logToFile) {
        i(tag, message, false, logToFile);
    }

    public static final void i(String tag, String message, boolean showStackTrace, boolean logToFile) {
        if (sEnable) {
            if (showStackTrace) {
                Log.i(tag, generateStackTraceMessage(message));
            } else {
                Log.i(tag, message);
            }

            if (logToFile) {
                logToFile(tag + " I " + message);
            }
        }
    }

    public static final void v(String tag, String message) {
        v(tag, message, true);
    }

    public static final void v(String tag, String message, boolean logToFile) {
        v(tag, message, false, logToFile);
    }

    public static final void v(String tag, String message, boolean showStackTrace, boolean logToFile) {
        if (sEnable) {
            if (showStackTrace) {
                Log.v(tag, generateStackTraceMessage(message));
            } else {
                Log.v(tag, message);
            }

            if (logToFile) {
                logToFile(tag + " V " + message);
            }
        }
    }

    private static final String generateStackTraceMessage(String message) {
        StringBuilder sb = new StringBuilder();
        StackTraceElement ste = Thread.currentThread().getStackTrace()[4];
        sb.append("[");
        sb.append(ste.getFileName().replace(".java", ""));
        sb.append("::");
        sb.append(ste.getMethodName());
        sb.append("]");
        sb.append(message);
        return sb.toString();
    }

    public static void createLogFile() {
        try {
            sLogFile = null;

            // directory
            String dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/cgv_cms_log";
            File dirFile = new File(dirPath);
            if (!dirFile.exists()) {
                if (!dirFile.mkdir()) {
                    return;
                }
            }

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd_HH_mm_ss");
            String logFileName = "log_" + simpleDateFormat.format(new Date()) + ".txt";
            sLogFile = new File(dirFile, logFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void logToFile(String log) {
        try {
            if (sLogFile == null) {
                return;
            }

            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(sLogFile, true));
            bufferedWriter.append(log);
            bufferedWriter.newLine();
            bufferedWriter.newLine();
            bufferedWriter.newLine();
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
