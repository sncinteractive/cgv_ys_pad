package com.cgv.ys.manager.pad.cgv_ys.view.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.cgv.ys.manager.pad.cgv_ys.R;
import com.cgv.ys.manager.pad.cgv_ys.common.AlertDialogButtonCallback;
import com.cgv.ys.manager.pad.cgv_ys.common.DebugLog;
import com.cgv.ys.manager.pad.cgv_ys.common.Defines;
import com.cgv.ys.manager.pad.cgv_ys.common.Enums;
import com.cgv.ys.manager.pad.cgv_ys.common.Util;
import com.cgv.ys.manager.pad.cgv_ys.model.CameraInfoModel;
import com.cgv.ys.manager.pad.cgv_ys.model.UserInfoModel;
import com.cgv.ys.manager.pad.cgv_ys.view.fragment.AZoneLiveScheduleFragment;
import com.cgv.ys.manager.pad.cgv_ys.view.fragment.CameraControlFragment;
import com.cgv.ys.manager.pad.cgv_ys.view.fragment.FragmentBase;
import com.cgv.ys.manager.pad.cgv_ys.view.fragment.InputSourceRoutingFragment;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MainActivity extends AppCompatActivity
    implements View.OnClickListener,
    FragmentBase.OnFragmentBaseInteractionListener,
    CameraControlFragment.OnFragmentInteractionListener,
    InputSourceRoutingFragment.OnFragmentInteractionListener,
    AZoneLiveScheduleFragment.OnFragmentInteractionListener {

    private static final String TAG = "MainActivity";

    private static final int REQ_CODE_PERMISSION_WRITE_EXTERNAL_STORAGE = 1000;

    //region model
    private UserInfoModel mUserInfoModel;
    //endregion model

    private MqttAndroidClient mMqttAndroidClient;
    private boolean mIsExplicitDisconnect;
    private boolean mIsConnectingMqtt;

    //region fragment
    private CameraControlFragment mCameraControlFragment;
    private AZoneLiveScheduleFragment mAZoneLiveScheduleFragment;
    private InputSourceRoutingFragment mInputSourceRoutingFragment;
    //endregion fragment

    private RelativeLayout mLoadingProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            ApplicationInfo applicationInfo = getApplicationInfo();
            boolean appDebuggable = (applicationInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
            DebugLog.setEnable(appDebuggable);

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            initializeView();

            if (DebugLog.isEnable()) {
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    String[] permissions = new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"};
                    requestPermissions(permissions, REQ_CODE_PERMISSION_WRITE_EXTERNAL_STORAGE);
                } else {
                    DebugLog.createLogFile();
                    startLogin();
                }
            } else {
                startLogin();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startLogin() {
        if (checkLoggedIn()) {
            initialize();
        } else {
            startActivityForResult(new Intent(this, LoginActivity.class), Defines.REQUEST_CODE_LOGIN);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQ_CODE_PERMISSION_WRITE_EXTERNAL_STORAGE: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (DebugLog.isEnable()) {
                        DebugLog.createLogFile();
                    }
                }

                startLogin();
            }
            break;
            default:
                break;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        try {
            connectMQTT();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        try {
            if (mMqttAndroidClient != null) {
                mIsExplicitDisconnect = true;
                mMqttAndroidClient.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initialize() {
        mCameraControlFragment = new CameraControlFragment();
        mAZoneLiveScheduleFragment = new AZoneLiveScheduleFragment();
        mInputSourceRoutingFragment = new InputSourceRoutingFragment();

        initializeMQTT();
        connectMQTT();
    }

    private boolean checkLoggedIn() {
        //mUserInfoModel = new UserInfoModel("id", "name", "pw");
        return (mUserInfoModel != null);

//        SharedPreferences prefs = Util.getSharedPreferences(this);
//        String userId = prefs.getString(Defines.PREFERENCES_KEY_USER_ID, null);
//        return (userId != null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case Defines.REQUEST_CODE_LOGIN: {
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        mUserInfoModel = data.getParcelableExtra("userInfo");

//                        SharedPreferences prefs = Util.getSharedPreferences(this);
//                        SharedPreferences.Editor editor = prefs.edit();
//                        editor.putString(Defines.PREFERENCES_KEY_USER_ID, mUserInfoModel.getUserID());
//                        editor.putString(Defines.PREFERENCES_KEY_USER_PW, mUserInfoModel.getUserPW());
//                        editor.apply();

                        initialize();
                    }
                } else {
                    finish();
                }
            }
            break;
        }
    }

    public UserInfoModel getUserInfoModel() {
        return mUserInfoModel;
    }

    private void initializeView() {
        try {
//            Button cameraControlTabButton = (Button) findViewById(R.id.ib_tab_camera_control);
//            cameraControlTabButton.setOnClickListener(this);
//            Button aZoneLiveScheduleTabButton = (Button) findViewById(R.id.ib_tab_a_zone_live_schedule);
//            aZoneLiveScheduleTabButton.setOnClickListener(this);
//            Button inputSourceRoutingTabButton = (Button) findViewById(R.id.ib_tab_input_source_routing);
//            inputSourceRoutingTabButton.setOnClickListener(this);

            mLoadingProgressBar = (RelativeLayout) findViewById(R.id.loading_progress);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showLoadingProgressBar() {
        mLoadingProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoadingProgressBar() {
        mLoadingProgressBar.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        try {
            mMqttAndroidClient = null;

            mUserInfoModel = null;
//            SharedPreferences prefs = Util.getSharedPreferences(this);
//            SharedPreferences.Editor editor = prefs.edit();
//            editor.putString(Defines.PREFERENCES_KEY_USER_ID, null);
//            editor.putString(Defines.PREFERENCES_KEY_USER_PW, null);
//            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }

        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_logout: {

            }
            break;
            case R.id.ib_tab_camera_control: {
                Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fl_main);
                if (!currentFragment.equals(mCameraControlFragment)) {
                    replaceFragment(mCameraControlFragment);
                }
            }
            break;
            case R.id.ib_tab_a_zone_live_schedule: {
                Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fl_main);
                if (!currentFragment.equals(mAZoneLiveScheduleFragment)) {
                    replaceFragment(mAZoneLiveScheduleFragment);
                }
            }
            break;
            case R.id.ib_tab_input_source_routing: {
                Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fl_main);
                if (!currentFragment.equals(mInputSourceRoutingFragment)) {
                    replaceFragment(mInputSourceRoutingFragment);
                }
            }
            break;
        }
    }

    @Override
    public void showMainActiviyLoadingProgressBar() {
        showLoadingProgressBar();
    }

    @Override
    public void hideMainAcitivyLoadingProgressBar() {
        hideLoadingProgressBar();
    }

    public void onChangeTab(Enums.TabType tabType) {
        Fragment newTabFragment = null;
        switch (tabType) {
            case CAMERA_CONTROL: {
                newTabFragment = mCameraControlFragment;
            }
            break;
            case LIVE_SCHEDULE: {
                newTabFragment = mAZoneLiveScheduleFragment;
            }
            break;
            case INPUT_SOURCE_ROUTING: {
                newTabFragment = mInputSourceRoutingFragment;
            }
            break;
            default:
                break;
        }
        if (newTabFragment == null) {
            return;
        } else {
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fl_main);
            if (currentFragment != null && currentFragment.equals(newTabFragment)) {
                return;
            }
        }

        Bundle args = new Bundle();
        args.putParcelable("userInfo", mUserInfoModel);
        newTabFragment.setArguments(args);

        DebugLog.e(TAG, "onChangeTab to " + newTabFragment.toString());
        replaceFragment(newTabFragment);
    }

    @Override
    public void onLogout() {
        try {
            mUserInfoModel = null;

//            SharedPreferences prefs = Util.getSharedPreferences(this);
//            SharedPreferences.Editor editor = prefs.edit();
//            editor.putString(Defines.PREFERENCES_KEY_USER_ID, null);
//            editor.putString(Defines.PREFERENCES_KEY_USER_PW, null);
//            editor.apply();

            startActivityForResult(new Intent(this, LoginActivity.class), Defines.REQUEST_CODE_LOGIN);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void replaceFragment(Fragment fragment) {
        try {
            //getSupportFragmentManager().beginTransaction().replace(R.id.fl_main, fragment).addToBackStack().commit();
            getSupportFragmentManager().beginTransaction().replace(R.id.fl_main, fragment).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeMQTT() {
        try {
            String serverUri = "tcp://61.73.59.92:1883";
            String clientId = "androidClient" + System.currentTimeMillis();
            mMqttAndroidClient = new MqttAndroidClient(getApplicationContext(), serverUri, clientId);
            mMqttAndroidClient.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {
                    DebugLog.e("MQTT", "connection lost");
                    if (!mIsExplicitDisconnect) {
                        connectMQTT();
                    }
                    mIsExplicitDisconnect = false;
                }

                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    DebugLog.e("MQTT", "message arrived. topic: " + topic + ", message: " + new String(message.getPayload()));
                    onReceivedMqttMessage(topic, new String(message.getPayload()));
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {
                    DebugLog.e("MQTT", "delivery complete");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void connectMQTT() {
        try {
            if (mMqttAndroidClient == null) {
                return;
            }

            DebugLog.e("MQTT", "connectMQTT - isConnected ? " + mMqttAndroidClient.isConnected() + ", isConnecting ? " + mIsConnectingMqtt);
            if (mMqttAndroidClient.isConnected() || mIsConnectingMqtt) {
                return;
            }

            showLoadingProgressBar();

            MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
            mqttConnectOptions.setAutomaticReconnect(false);
            mqttConnectOptions.setCleanSession(false);

            mIsConnectingMqtt = true;

            mMqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    System.err.println("mqtt connect success");

                    mIsConnectingMqtt = false;

                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    mMqttAndroidClient.setBufferOpts(disconnectedBufferOptions);

                    try {
                        mMqttAndroidClient.subscribe(Defines.SUBSCRIBE_TOPIC, 0, null, new IMqttActionListener() {
                            @Override
                            public void onSuccess(IMqttToken asyncActionToken) {
                                hideLoadingProgressBar();
                                onChangeTab(Enums.TabType.LIVE_SCHEDULE);
                                DebugLog.e("MQTT", "subscribe success");
                            }

                            @Override
                            public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                                DebugLog.e("MQTT", "mqtt subscribe fail. cause: " + exception.getMessage());

                                hideLoadingProgressBar();

                                Util.showAlertDialog(MainActivity.this,
                                    getString(R.string.dialog_title),
                                    getString(R.string.dialog_message_connect_channel_fail, exception.getMessage()),
                                    getString(R.string.finish),
                                    null,
                                    new AlertDialogButtonCallback() {
                                        @Override
                                        public void onClickButton(boolean isPositiveClick) {
                                            if (isPositiveClick) {
                                                finish();
                                            }
                                        }
                                    }

                                );
                            }
                        });

//                        // THIS DOES NOT WORK!
//                        mMqttAndroidClient.subscribe(SUBSCRIBE_TOPIC, 0, new IMqttMessageListener() {
//                            @Override
//                            public void messageArrived(String topic, MqttMessage message) throws Exception {
//                                // message Arrived!
//                                System.err.println("Message: " + topic + " : " + new String(message.getPayload()));
//                            }
//                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    DebugLog.e("MQTT", "mqtt connection fail. cause: " + exception.getMessage());

                    mIsConnectingMqtt = false;

                    hideLoadingProgressBar();

                    Util.showAlertDialog(MainActivity.this,
                        getString(R.string.dialog_title),
                        getString(R.string.dialog_message_connect_mqtt_fail, exception.getMessage()),
                        getString(R.string.finish),
                        null,
                        new AlertDialogButtonCallback() {
                            @Override
                            public void onClickButton(boolean isPositiveClick) {
                                if (isPositiveClick) {
                                    finish();
                                }
                            }
                        }
                    );
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void publishMessage(String publishMessage) {
        try {
            if (mMqttAndroidClient != null && mMqttAndroidClient.isConnected()) {
                MqttMessage mqttMessage = new MqttMessage();
                mqttMessage.setPayload(publishMessage.getBytes());
                mMqttAndroidClient.publish(Defines.PUBLISH_TOPIC, mqttMessage);
                DebugLog.e("MQTT", "publish success. message => " + publishMessage);
            }
        } catch (MqttException e) {
            DebugLog.e("MQTT", "publish exception. message => " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void onReceivedMqttMessage(String topic, String message) {
        try {
            if (!topic.equals(Defines.SUBSCRIBE_TOPIC)) {
                return;
            }

            // message sample (command|data)
            // CamPanPlus|-90,12 (pan,tilt)
            // CamPanMinus|-90,9 (pan,tilt)
            // CamTiltPlus|-87,9 (pan,tilt)
            // CamTiltMinus|-90,9 (pan,tilt)
            // CamZoomPlus|15 (zoom)
            // CamZoomMinus|12 (zoom)
            // CamStatus|-175,-90,12 (tilt, pan, zoom)

            String[] splitMessages = message.split("\\|");
            if (splitMessages.length != 2) {
                return;
            }

            String command = splitMessages[0];
            String[] datas = splitMessages[1].split(",");

            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fl_main);
            if (currentFragment != null) {
                if (currentFragment.equals(mCameraControlFragment)) {
                    if (command.equals("CamPanPlus") ||
                        command.equals("CamPanMinus") ||
                        command.equals("CamTiltPlus") ||
                        command.equals("CamTiltMinus")) {
                        if (datas.length >= 2) {
                            mCameraControlFragment.applyCameraInfoToModel(Enums.CommandOption.TILT, Integer.parseInt(datas[0]));
                            mCameraControlFragment.applyCameraInfoToModel(Enums.CommandOption.PAN, Integer.parseInt(datas[1]));
                            mCameraControlFragment.applyCameraInfoToUI();
                        }
                    } else if (command.equals("CamZoomPlus") || command.equals("CamZoomMinus")) {
                        if (datas.length >= 1) {
                            mCameraControlFragment.applyCameraInfoToModel(Enums.CommandOption.ZOOM, Integer.parseInt(datas[0]));
                            mCameraControlFragment.applyCameraInfoToUI();
                        }
                    } else if (command.equals("CamStatus")) {
                        hideLoadingProgressBar();

                        if (datas.length >= 3) {
                            mCameraControlFragment.setCameraInfoModel(new CameraInfoModel(Integer.parseInt(datas[0]), Integer.parseInt(datas[1]), Integer.parseInt(datas[2])));
                            mCameraControlFragment.applyCameraInfoToUI();
                        }
                    } else if (command.equals("CamZoom")) {
                        if (datas.length >= 1) {
                            mCameraControlFragment.applyCameraInfoToModel(Enums.CommandOption.ZOOM, Integer.parseInt(datas[0]));
                            mCameraControlFragment.applyCameraInfoToUI();
                        }
                    } else if (command.equals("CamPan") || command.equals("CamTilt")) {
                        if (datas.length >= 2) {
                            mCameraControlFragment.applyCameraInfoToModel(Enums.CommandOption.TILT, Integer.parseInt(datas[0]));
                            mCameraControlFragment.applyCameraInfoToModel(Enums.CommandOption.PAN, Integer.parseInt(datas[1]));
                            mCameraControlFragment.applyCameraInfoToUI();
                        }
                    }
                } else if (currentFragment.equals(mInputSourceRoutingFragment)) {
                    if (command.equals("RouteMode")) {
                        if (datas.length >= 1) {
                            mInputSourceRoutingFragment.changeRouteMode(datas[0]);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        Util.showAlertDialog(this,
            getString(R.string.dialog_title),
            getString(R.string.dialog_message_finish_app),
            getString(R.string.finish),
            getString(R.string.cancel),
            new AlertDialogButtonCallback() {
                @Override
                public void onClickButton(boolean isPositiveClick) {
                    if (isPositiveClick) {
                        finish();
                    }
                }
            }
        );
    }

    @Override
    public void onRequestMqttCameraStatus(int cameraIndex) {
        showLoadingProgressBar();

        publishMessage("CamStatus|Cam" + Integer.toString(cameraIndex + 1));
    }

    @Override
    public void onRequestMqttCameraUpDown(int cameraIndex, Enums.CommandOption commandOption, Enums.CommandAction commandAction) {
        try {
            if (commandAction == Enums.CommandAction.SET) {
                return;
            }

            // ex) "CamZoomPlus|Cam1"
            StringBuilder builder = new StringBuilder();
            // command
            builder.append(Enums.CommandType.CAM.getValue());
            builder.append(commandOption.getValue());
            builder.append(commandAction.getValue());
            // separator
            builder.append("|");
            // data
            builder.append("Cam");
            builder.append(cameraIndex + 1);

            publishMessage(builder.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestMqttCameraSet(int cameraIndex, Enums.CommandOption commandOption, int value) {
        try {
            // ex) "CamZoom|Cam1,12"
            StringBuilder builder = new StringBuilder();
            // command
            builder.append(Enums.CommandType.CAM.getValue());
            builder.append(commandOption.getValue());
            // separator
            builder.append("|");
            // data
            builder.append("Cam");
            builder.append(cameraIndex + 1);
            builder.append(",");

            builder.append(Integer.toString(value));

            publishMessage(builder.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestMqttApplyManualRouting(String message) {
        publishMessage(message);
    }

    @Override
    public void onRequestMqttSetRouteMode(String message) {
        DebugLog.e(TAG, "onRequestMqttSetRouteMode. message = " + message);

        publishMessage(message);
    }

    public void onRequestMqttLiveSchedule(String message) {
        publishMessage(message);
    }

    // 프래그먼트 교체시 백스택에 추가하는 경우 백버튼 눌렀을때 백스택 체크해서 초기화면까지 돌아오면 앱 종료 다이얼로그를 띄움.
    // 기능 사용하려면 replaceFragment에 주석처리한 addToBackStack 호출을 살리면 됨.
//    @Override
//    public void onBackPressed() {
//        try {
//            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
//                super.onBackPressed();
//            } else {
//                Util.showAlertDialog(this,
//                    getString(R.string.dialog_title),
//                    getString(R.string.dialog_message_finish_app),
//                    getString(R.string.finish),
//                    getString(R.string.cancel),
//                    new AlertDialogButtonCallback() {
//                        @Override
//                        public void onClickButton(boolean isPositiveClick) {
//                            if (isPositiveClick) {
//                                finish();
//                            }
//                        }
//                    }
//                );
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
}
