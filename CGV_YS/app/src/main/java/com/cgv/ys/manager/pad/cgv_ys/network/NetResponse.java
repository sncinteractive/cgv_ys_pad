package com.cgv.ys.manager.pad.cgv_ys.network;

/**
 * Created by eklee on 2017. 6. 13..
 */

public interface NetResponse {
    void onResponse(String jsonResponse);
}
