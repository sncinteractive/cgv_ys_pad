package com.cgv.ys.manager.pad.cgv_ys.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eklee on 2017. 6. 14..
 */

public class CameraInfoModel {
    @SerializedName("tilt")
    private int mTilt;

    @SerializedName("pan")
    private int mPan;

    @SerializedName("zoom")
    private int mZoom;

    public CameraInfoModel() {
    }

    public CameraInfoModel(int tilt, int pan, int zoom) {
        mTilt = tilt;
        mPan = pan;
        mZoom = zoom;
    }

    public int getTilt() {
        return mTilt;
    }

    public void setTilt(int tilt) {
        mTilt = tilt;
    }

    public int getPan() {
        return mPan;
    }

    public void setPan(int pan) {
        mPan = pan;
    }

    public int getZoom() {
        return mZoom;
    }

    public void setZoom(int zoom) {
        mZoom = zoom;
    }
}
