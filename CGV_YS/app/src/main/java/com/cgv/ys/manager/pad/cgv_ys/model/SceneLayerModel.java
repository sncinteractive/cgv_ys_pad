package com.cgv.ys.manager.pad.cgv_ys.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eklee on 2017. 6. 20..
 */

public class SceneLayerModel {
    @SerializedName("SHL_Index")
    private int mIndex;

    @SerializedName("SHL_Type")
    private String mType;

    @SerializedName("SHL_Col")
    private int mCol;

    @SerializedName("SHL_Row")
    private int mRow;

    @SerializedName("SHL_Width")
    private int mWidth;

    @SerializedName("SHL_Height")
    private int mHeight;

    @SerializedName("Content")
    private LayerContent mLayerContent;

    public SceneLayerModel() {
    }

    public SceneLayerModel(int index, String type, int col, int row, int width, int height, LayerContent layerContent) {
        mIndex = index;
        mType = type;
        mCol = col;
        mRow = row;
        mWidth = width;
        mHeight = height;
        mLayerContent = layerContent;
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public int getCol() {
        return mCol;
    }

    public void setCol(int col) {
        mCol = col;
    }

    public int getRow() {
        return mRow;
    }

    public void setRow(int row) {
        mRow = row;
    }

    public int getWidth() {
        return mWidth;
    }

    public void setWidth(int width) {
        mWidth = width;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int height) {
        mHeight = height;
    }

    public LayerContent getLayerContent() {
        return mLayerContent;
    }

    public void setLayerContent(LayerContent layerContent) {
        mLayerContent = layerContent;
    }
}
