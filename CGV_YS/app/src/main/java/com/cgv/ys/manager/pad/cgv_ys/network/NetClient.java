package com.cgv.ys.manager.pad.cgv_ys.network;

import android.content.Context;

import com.cgv.ys.manager.pad.cgv_ys.common.DebugLog;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HeaderElement;
import cz.msebera.android.httpclient.ParseException;

/**
 * Created by eklee on 2017. 6. 13..
 */

public class NetClient {
    private static final String TAG = "NetClient";
    private static final String HOST_URL = "http://116.121.35.49/iface";

    private static AsyncHttpClient sAsyncHttpClient;

    private static Header[] sHeaders;

    private static AsyncHttpClient getAsyncHttpClient() {
        try {
            if (sAsyncHttpClient == null) {
                sAsyncHttpClient = new AsyncHttpClient();
            }

            if (sHeaders == null) {
                List<Header> headerList = new ArrayList<>();
                headerList.add(new Header() {
                    @Override
                    public String getName() {
                        return "X-DEVICE-ID";
                    }

                    @Override
                    public String getValue() {
                        return "android-pad";
                    }

                    @Override
                    public HeaderElement[] getElements() throws ParseException {
                        return new HeaderElement[0];
                    }
                });
                sHeaders = headerList.toArray(new Header[(headerList.size())]);
            }

            sAsyncHttpClient.setLoggingEnabled(false);
            sAsyncHttpClient.setTimeout(20 * 1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sAsyncHttpClient;
    }

    public static void get(Context context, final String url, final RequestParams requestParams, final NetResponseCallback callback) {
        AsyncHttpClient asyncHttpClient = getAsyncHttpClient();

        asyncHttpClient.get(context, url, null, requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                DebugLog.e(TAG, "get success. url => " + getRelativeURL(url) + ", reqParams => " + (requestParams != null ? requestParams.toString() : "null") + ", response => " + new String(responseBody));
                callback.onResponse(new String(responseBody));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                DebugLog.e(TAG, "get fail. url => " + getRelativeURL(url) + ", reqParams => " + (requestParams != null ? requestParams.toString() : "null") + ", error => " + error.getMessage());
                callback.onResponse(null);
            }
        });
    }

    public static void post(Context context, final String url, final RequestParams requestParams, final NetResponseCallback callback) {
        AsyncHttpClient asyncHttpClient = getAsyncHttpClient();
        asyncHttpClient.post(context, getRelativeURL(url), sHeaders, requestParams, "application/x-www-form-urlencoded", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                DebugLog.e(TAG, "post success. url => " + getRelativeURL(url) + ", reqParams => " + (requestParams != null ? requestParams.toString() : "null") + ", response => " + new String(responseBody));
                callback.onResponse(new String(responseBody));
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                DebugLog.e(TAG, "post fail. url => " + getRelativeURL(url) + ", reqParams => " + (requestParams != null ? requestParams.toString() : "null") + ", error => " + error.getMessage());
                callback.onResponse(null);
            }
        });
    }

    private static String getRelativeURL(String url) {
        return HOST_URL + url;
    }
}
