package com.cgv.ys.manager.pad.cgv_ys.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by eklee on 2017. 6. 20..
 */

public class SceneModel {
    @SerializedName("SHS_Index")
    private int mIndex;

    @SerializedName("SHS_Name")
    private String mName; // ex "1. Live+포스터+Live"

    @SerializedName("Layers")
    private List<SceneLayerModel> mSceneLayerModelList; // 고정값 video layer 3개, audio layer 1개

    private boolean mIsApplied;

    private boolean mIsPreviewed;

    public SceneModel() {
    }

    public SceneModel(int index, String name, List<SceneLayerModel> sceneLayerModelList, boolean isApplied, boolean isPreviewed) {
        mIndex = index;
        mName = name;
        mSceneLayerModelList = sceneLayerModelList;
        mIsApplied = isApplied;
        mIsPreviewed = isPreviewed;
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public List<SceneLayerModel> getSceneLayerModelList() {
        return mSceneLayerModelList;
    }

    public void setSceneLayerModelList(List<SceneLayerModel> sceneLayerModelList) {
        mSceneLayerModelList = sceneLayerModelList;
    }

    public boolean isApplied() {
        return mIsApplied;
    }

    public void setApplied(boolean applied) {
        mIsApplied = applied;
    }

    public boolean isPreviewed() {
        return mIsPreviewed;
    }

    public void setPreviewed(boolean previewed) {
        mIsPreviewed = previewed;
    }
}
