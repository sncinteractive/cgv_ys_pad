package com.cgv.ys.manager.pad.cgv_ys.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eklee on 2017. 6. 16..
 */

public class RouteDetailModel {
    @SerializedName("IOD_IORIndex")
    private int mRouteIndex; // 라우팅 리스트 인덱스

    @SerializedName("IOD_IORType")
    private String mRouteType; // 라우팅 타입: Video, Audio

    @SerializedName("IOD_IOSIndexOutput")
    private String mIndexOutput; // 장치 인덱스 아웃풋 코드

    @SerializedName("IOD_IOSInputCode")
    private String mInputCode; // 입력 코드: mqtt 전송에 사용

    @SerializedName("IOD_IOSOutputCode")
    private String mOutputCode; // 출력 코드: mqtt 전송에 사용

    @SerializedName("IOS_Device")
    private String mDeviceType; // 입력 장치 이름: Video인 경우 "PC, CAM, ENG", Audio인 경우 "MIC, SPK, MUTE"

    @SerializedName("IOS_Name")
    private String mDeviceName; // 입력 장치 이름: 데이터 그대로 사용

    public RouteDetailModel() {
    }

    public RouteDetailModel(int routeIndex, String routeType, String indexOutput, String inputCode, String outputCode, String deviceType, String deviceName) {
        mRouteIndex = routeIndex;
        mRouteType = routeType;
        mIndexOutput = indexOutput;
        mInputCode = inputCode;
        mOutputCode = outputCode;
        mDeviceType = deviceType;
        mDeviceName = deviceName;
    }

    public int getRouteIndex() {
        return mRouteIndex;
    }

    public void setRouteIndex(int routeIndex) {
        mRouteIndex = routeIndex;
    }

    public String getRouteType() {
        return mRouteType;
    }

    public void setRouteType(String routeType) {
        mRouteType = routeType;
    }

    public String getIndexOutput() {
        return mIndexOutput;
    }

    public void setIndexOutput(String indexOutput) {
        mIndexOutput = indexOutput;
    }

    public String getInputCode() {
        return mInputCode;
    }

    public void setInputCode(String inputCode) {
        mInputCode = inputCode;
    }

    public String getOutputCode() {
        return mOutputCode;
    }

    public void setOutputCode(String outputCode) {
        mOutputCode = outputCode;
    }

    public String getDeviceType() {
        return mDeviceType;
    }

    public void setDeviceType(String deviceType) {
        mDeviceType = deviceType;
    }

    public String getDeviceName() {
        return mDeviceName;
    }

    public void setDeviceName(String deviceName) {
        mDeviceName = deviceName;
    }
}
