package com.cgv.ys.manager.pad.cgv_ys.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cgv.ys.manager.pad.cgv_ys.R;
import com.cgv.ys.manager.pad.cgv_ys.model.SceneModel;

import java.util.List;

/**
 * Created by eklee on 2017. 6. 16..
 */

public class SceneListAdapter extends RecyclerView.Adapter<SceneListAdapter.ViewHolder> {
    public interface ItemInteractionListener {
        void onApply(int position);

        void onPreview(int position);
    }

    private List<SceneModel> mSceneModelList;

    private ItemInteractionListener mItemInteractionListener;
    private final SceneListAdapter.ViewHolder.ViewHolderInteractionListener mViewHolderInteractionListener = new ViewHolderInteractionListenerImpl();

    public SceneListAdapter(List<SceneModel> titleList, ItemInteractionListener listener) {
        mSceneModelList = titleList;
        mItemInteractionListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_scene_list, parent, false);
        return new ViewHolder(view, mViewHolderInteractionListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            SceneModel model = mSceneModelList.get(position);
            if (model != null) {
                holder.mTitleTextView.setText(model.getName());
                holder.itemView.setSelected(model.isPreviewed());
                holder.mTitleTextView.setSelected(model.isPreviewed());
                holder.mAppliedIconImageView.setVisibility(model.isApplied() ? View.VISIBLE : View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mSceneModelList.size();
    }

    public class ViewHolderInteractionListenerImpl implements ViewHolder.ViewHolderInteractionListener {

        @Override
        public void onApply(int position) {
            mItemInteractionListener.onApply(position);
        }

        @Override
        public void onPreview(int position) {
            mItemInteractionListener.onPreview(position);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public interface ViewHolderInteractionListener {
            void onApply(int position);

            void onPreview(int position);
        }

        private TextView mTitleTextView;
        private ImageView mAppliedIconImageView;
        private ViewHolderInteractionListener mInteractionListener;

        public ViewHolder(View itemView, ViewHolderInteractionListener listener) {
            super(itemView);

            mTitleTextView = (TextView) itemView.findViewById(R.id.tv_title);
            mAppliedIconImageView = (ImageView) itemView.findViewById(R.id.iv_applied);
            Button previewButton = (Button) itemView.findViewById(R.id.btn_preview);
            previewButton.setOnClickListener(this);
            Button applyButton = (Button) itemView.findViewById(R.id.btn_apply);
            applyButton.setOnClickListener(this);

            mInteractionListener = listener;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_preview: {
                    if (mInteractionListener != null) {
                        mInteractionListener.onPreview(getLayoutPosition());
                    }
                }
                break;
                case R.id.btn_apply: {
                    if (mInteractionListener != null) {
                        mInteractionListener.onApply(getLayoutPosition());
                    }
                }
                break;
                default:
                    break;
            }
        }
    }
}
