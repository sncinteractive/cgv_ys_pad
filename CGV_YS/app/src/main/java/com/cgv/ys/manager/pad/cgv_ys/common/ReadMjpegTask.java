package com.cgv.ys.manager.pad.cgv_ys.common;

import android.os.AsyncTask;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by eklee on 2017. 6. 24..
 */

public class ReadMjpegTask extends AsyncTask<Integer, Void, MjpegInputStream> {

    private static final String TAG = "ReadMjpegTask";

    private MjpegView mMjpegView;

    public static HttpURLConnection[] mConnectionList = new HttpURLConnection[3];
    public static final String[] CAM_URL_ARRAY = new String[]{
        "http://61.73.59.92:20110/cgi-bin/mjpeg?resolution=640x360&framerate=30&quality=1",
        "http://61.73.59.92:20111/cgi-bin/mjpeg?resolution=640x360&framerate=30&quality=1",
        "http://61.73.59.92:20112/cgi-bin/mjpeg?resolution=640x360&framerate=30&quality=1"
    };

    public ReadMjpegTask(MjpegView mjpegView) {
        mMjpegView = mjpegView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    protected MjpegInputStream doInBackground(Integer... urlIndex) {
        try {
            int index = urlIndex[0];
            if (mConnectionList[index] != null) {
                mConnectionList[index].disconnect();
                mConnectionList[index] = null;
            }

            URL url = new URL(CAM_URL_ARRAY[index]);
            mConnectionList[index] = (HttpURLConnection) url.openConnection();
            mConnectionList[index].setRequestMethod("GET");
            mConnectionList[index].setConnectTimeout(30 * 1000);
            mConnectionList[index].setReadTimeout(30 * 1000);
            mConnectionList[index].connect();

            DebugLog.e(TAG, "request finished");

            return new MjpegInputStream(mConnectionList[index].getInputStream());

        } catch (Exception e) {
            e.printStackTrace();
            DebugLog.e(TAG, "request failed with exception");
        }

        return null;

//        HttpResponse res;
//        DefaultHttpClient httpclient = new DefaultHttpClient();
//        try {
//            res = httpclient.execute(new HttpGet(URI.create(urlString[0])));
//            return new MjpegInputStream(res.getEntity().getContent());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return null;
    }

    protected void onPostExecute(MjpegInputStream result) {
        mMjpegView.setSource(result);
        mMjpegView.setDisplayMode(MjpegView.SIZE_BEST_FIT);
        mMjpegView.showFps(false);
    }

    public static void disconnectAll() {
        try {
            DebugLog.d(TAG, "disconnectAll http connection");
            for (int i = 0; i < mConnectionList.length; ++i) {
                if (mConnectionList[i] != null) {
                    mConnectionList[i].disconnect();
                    DebugLog.d(TAG, "disconnectAll http connection done. index = " + i);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            DebugLog.e(TAG, "disconnectAll");
        }
    }

    public static void disconnect(int index) {
        try {
            DebugLog.d(TAG, "disconnect http connection call. index = " + index);
            if (index >= 0 && index < mConnectionList.length) {
                if (mConnectionList[index] != null) {
                    mConnectionList[index].disconnect();
                    DebugLog.d(TAG, "disconnect http connection done. index = " + index);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            DebugLog.e(TAG, "disconnect index = " + index);
        }
    }
}
