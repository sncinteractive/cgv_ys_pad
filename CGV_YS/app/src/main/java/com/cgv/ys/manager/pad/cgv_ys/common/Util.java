package com.cgv.ys.manager.pad.cgv_ys.common;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;

/**
 * Created by eklee on 2017. 6. 14..
 */

public class Util {
    public static boolean isStringNullOrEmpty(String string) {
        if (string == null) {
            return true;
        }

        return string.isEmpty();
    }

    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(Defines.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public static void showAlertDialog(Context context,
                                       String title,
                                       String message,
                                       String positiveButtonTitle,
                                       String negativeButtonTitle,
                                       final AlertDialogButtonCallback callback) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title)
            .setMessage(message);

        if (!isStringNullOrEmpty(positiveButtonTitle)) {
            builder.setPositiveButton(positiveButtonTitle, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (callback != null) {
                        callback.onClickButton(true);
                    }
                    dialog.dismiss();
                }
            });
        }

        if (!isStringNullOrEmpty(negativeButtonTitle)) {
            builder.setNegativeButton(negativeButtonTitle, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (callback != null) {
                        callback.onClickButton(false);
                    }
                    dialog.dismiss();
                }
            });
        }

        builder.show();
    }

    /**
     * Gson jsonarry to array or list
     */
    // #1
//    MyClass[] array = gson.fromJson(jsonString, MyClass[].class);
//    List<MyClass> list = Arrays.asList(array);
//
//    // #2
//    List<MyClass> list2 = gson.fromJson(jsonString, new TypeToken<List<MyClass>>(){}.getType());
}
