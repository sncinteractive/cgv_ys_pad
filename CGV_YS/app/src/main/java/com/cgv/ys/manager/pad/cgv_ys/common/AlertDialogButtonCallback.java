package com.cgv.ys.manager.pad.cgv_ys.common;

/**
 * Created by eklee on 2017. 6. 16..
 */

public interface AlertDialogButtonCallback {
    void onClickButton(boolean isPositiveClick);
}
