package com.cgv.ys.manager.pad.cgv_ys.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.cgv.ys.manager.pad.cgv_ys.R;
import com.cgv.ys.manager.pad.cgv_ys.common.Enums;
import com.cgv.ys.manager.pad.cgv_ys.model.UserInfoModel;

/**
 * Created by eklee on 2017. 6. 20..
 */

public class FragmentBase extends Fragment implements View.OnClickListener {

    public interface OnFragmentBaseInteractionListener {
        void showMainActiviyLoadingProgressBar();
        void hideMainAcitivyLoadingProgressBar();

        void onChangeTab(Enums.TabType tabType);

        void onLogout();
    }

    private OnFragmentBaseInteractionListener mOnFragmentBaseInteractionListener;

    private ImageButton mTabCameraControlButton;
    private ImageButton mTabLiveScheduleButton;
    private ImageButton mTabInpoutSourceRoutingButton;

    private View mBackgroundView;

    public void initializeView(View view) {
        try {
            String userName = "";
            Bundle args = getArguments();
            if (args != null) {
                UserInfoModel userInfoModel = args.getParcelable("userInfo");
                if (userInfoModel != null) {
                    userName = userInfoModel.getUserName();
                }
            }


            TextView userNameTextView = (TextView) view.findViewById(R.id.tv_user_name);
            userNameTextView.setText(userName);

            Button logoutButton = (Button) view.findViewById(R.id.btn_logout);
            logoutButton.setOnClickListener(this);

            mTabCameraControlButton = (ImageButton) view.findViewById(R.id.ib_tab_camera_control);
            mTabCameraControlButton.setOnClickListener(this);
            mTabLiveScheduleButton = (ImageButton) view.findViewById(R.id.ib_tab_a_zone_live_schedule);
            mTabLiveScheduleButton.setOnClickListener(this);
            mTabInpoutSourceRoutingButton = (ImageButton) view.findViewById(R.id.ib_tab_input_source_routing);
            mTabInpoutSourceRoutingButton.setOnClickListener(this);

            mBackgroundView = view.findViewById(R.id.background_view);
            hideBackground();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentBaseInteractionListener) {
            mOnFragmentBaseInteractionListener = (OnFragmentBaseInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                + " must implement OnFragmentBaseInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mOnFragmentBaseInteractionListener = null;
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.btn_logout: {
                    mOnFragmentBaseInteractionListener.onLogout();
                }
                break;
                case R.id.ib_tab_camera_control: {
                    onClickTab(Enums.TabType.CAMERA_CONTROL);
                }
                break;
                case R.id.ib_tab_a_zone_live_schedule: {
                    onClickTab(Enums.TabType.LIVE_SCHEDULE);
                }
                break;
                case R.id.ib_tab_input_source_routing: {
                    onClickTab(Enums.TabType.INPUT_SOURCE_ROUTING);
                }
                break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onClickTab(Enums.TabType tabType) {
        mOnFragmentBaseInteractionListener.onChangeTab(tabType);
    }

    protected void showMainActiviyLoadingProgressBar() {
        mOnFragmentBaseInteractionListener.showMainActiviyLoadingProgressBar();
    }

    protected void hideMainAcitivyLoadingProgressBar() {
        mOnFragmentBaseInteractionListener.hideMainAcitivyLoadingProgressBar();
    }

    protected void showBackground() {
        if (mBackgroundView != null)
            mBackgroundView.setVisibility(View.VISIBLE);
    }

    protected void hideBackground() {
        if (mBackgroundView != null)
            mBackgroundView.setVisibility(View.GONE);
    }

    protected void setSelectedTabButton(Enums.TabType tabType) {
        mTabCameraControlButton.setSelected(false);
        mTabLiveScheduleButton.setSelected(false);
        mTabInpoutSourceRoutingButton.setSelected(false);

        switch (tabType) {
            case CAMERA_CONTROL: {
                mTabCameraControlButton.setSelected(true);
            }
            break;
            case LIVE_SCHEDULE: {
                mTabLiveScheduleButton.setSelected(true);
            }
            break;
            case INPUT_SOURCE_ROUTING: {
                mTabInpoutSourceRoutingButton.setSelected(true);
            }
            break;
            default:
                break;
        }
    }
}
