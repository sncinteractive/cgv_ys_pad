package com.cgv.ys.manager.pad.cgv_ys.model;

import java.util.List;

/**
 * Created by eklee on 2017. 6. 16..
 */

public class RoutePreviewModel {
    private RouteListItemModel mRouteListItemModel;
    private List<RouteDetailModel> mRouteDetailModelList;

    public RoutePreviewModel() {
    }

    public RouteListItemModel getRouteListItemModel() {
        return mRouteListItemModel;
    }

    public void setRouteListItemModel(RouteListItemModel routeListItemModel) {
        mRouteListItemModel = routeListItemModel;
    }

    public List<RouteDetailModel> getRouteDetailModelList() {
        return mRouteDetailModelList;
    }

    public void setRouteDetailModelList(List<RouteDetailModel> routeDetailModelList) {
        mRouteDetailModelList = routeDetailModelList;
    }
}
