package com.cgv.ys.manager.pad.cgv_ys.view.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cgv.ys.manager.pad.cgv_ys.R;
import com.cgv.ys.manager.pad.cgv_ys.common.AlertDialogButtonCallback;
import com.cgv.ys.manager.pad.cgv_ys.common.DebugLog;
import com.cgv.ys.manager.pad.cgv_ys.common.Defines;
import com.cgv.ys.manager.pad.cgv_ys.common.Enums;
import com.cgv.ys.manager.pad.cgv_ys.common.Util;
import com.cgv.ys.manager.pad.cgv_ys.model.RouteDetailModel;
import com.cgv.ys.manager.pad.cgv_ys.model.RouteListItemModel;
import com.cgv.ys.manager.pad.cgv_ys.model.RouteModeModel;
import com.cgv.ys.manager.pad.cgv_ys.model.RoutePreviewModel;
import com.cgv.ys.manager.pad.cgv_ys.network.NetClient;
import com.cgv.ys.manager.pad.cgv_ys.network.NetResponse;
import com.cgv.ys.manager.pad.cgv_ys.network.NetResponseCallback;
import com.cgv.ys.manager.pad.cgv_ys.view.ViewHolder.AudioOutputViewHolder;
import com.cgv.ys.manager.pad.cgv_ys.view.ViewHolder.RouteOutputViewHolder;
import com.cgv.ys.manager.pad.cgv_ys.view.adapter.RoutingListAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class InputSourceRoutingFragment extends FragmentBase
    implements View.OnClickListener {

    public interface OnFragmentInteractionListener {
        void onRequestMqttApplyManualRouting(String message);

        void onRequestMqttSetRouteMode(String message);
    }

    private static final String TAG = "InputSourceRoutingFragment";
    public static final int MAX_VIDEO_OUTPUT_COUNT = 22;
    public static final int MAX_AUDIO_OUTPUT_COUNT = 4;

    private OnFragmentInteractionListener mOnFragmentInteractionListener;

    private RouteModeModel mRouteModeModel;
    private List<RouteListItemModel> mRouteListItemModelList;

    private RouteListItemModel mAppliedRouteListItemModel;
    private RoutePreviewModel mRoutePreviewModel;

    private View mViewMyself;

    private Button mRoutingModeAutoButton;
    private Button mRoutingModeManualButton;
	private Button mRoutingModeShowStartButton;
	private Button mRoutingModeShowStopButton;

	private boolean mIsRoutingModeShowStart;

    private RecyclerView mRoutingListRecyclerView;

    //region preview
    private RelativeLayout mPreviewLayout;
    private TextView mPreviewRouteName;
    private Button mPreviewVideoRouteButton;
    private Button mPreviewAudioRouteButton;

    private ViewGroup mVideoRouteOutput;
    private Map<String, RouteOutputViewHolder> mVideoOutputViewHolderMap;

    private ViewGroup mAudioRouteOutput;
    private Map<String, AudioOutputViewHolder> mAudioOutputViewHolderMap;
    //endregion preview

    public InputSourceRoutingFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            mViewMyself = inflater.inflate(R.layout.fragment_input_source_routing, container, false);

            super.initializeView(mViewMyself);

            setSelectedTabButton(Enums.TabType.INPUT_SOURCE_ROUTING);

            mRoutingModeAutoButton = (Button) mViewMyself.findViewById(R.id.btn_routing_mode_auto);
            mRoutingModeAutoButton.setOnClickListener(this);
            mRoutingModeManualButton = (Button) mViewMyself.findViewById(R.id.btn_routing_mode_manual);

	        mRoutingModeShowStartButton = (Button) mViewMyself.findViewById(R.id.btn_routing_mode_start);
	        mRoutingModeShowStartButton.setOnClickListener(this);
	        mRoutingModeShowStopButton = (Button) mViewMyself.findViewById(R.id.btn_routing_mode_stop);
	        mRoutingModeShowStopButton.setOnClickListener(this);

            mAppliedRouteListItemModel = null;
            mRoutePreviewModel = new RoutePreviewModel();
            mRouteListItemModelList = new ArrayList<>();
            mRoutingListRecyclerView = (RecyclerView) mViewMyself.findViewById(R.id.rv_routing_list);
            mRoutingListRecyclerView.setAdapter(new RoutingListAdapter(mRouteListItemModelList, new RoutingListItemInteractionListener()));
            //mRoutingListRecyclerView.setHasFixedSize(true);

            //region preview layout
            mPreviewLayout = (RelativeLayout) mViewMyself.findViewById(R.id.rl_preview_root);
            mPreviewRouteName = (TextView) mPreviewLayout.findViewById(R.id.tv_popup_preview_route_name);
            mPreviewVideoRouteButton = (Button) mPreviewLayout.findViewById(R.id.btn_video_route);
            mPreviewVideoRouteButton.setOnClickListener(this);
            mPreviewAudioRouteButton = (Button) mPreviewLayout.findViewById(R.id.btn_audio_route);
            mPreviewAudioRouteButton.setOnClickListener(this);

            mVideoRouteOutput = (ViewGroup) mPreviewLayout.findViewById(R.id.ll_video_route);
            mVideoOutputViewHolderMap = new HashMap<>(MAX_VIDEO_OUTPUT_COUNT);
            for (int i = 0; i < MAX_VIDEO_OUTPUT_COUNT; i++) {
                View routeOutputRoot = mVideoRouteOutput.findViewById(getVideoOutputResID(i));
                String outputIndex = String.format(Locale.ENGLISH, "%02d", Defines.VIDEO_ROUTE_INDEX_OUTPUT_BEGIN + i);
                mVideoOutputViewHolderMap.put(outputIndex, RouteOutputViewHolder.createInstance(routeOutputRoot));
            }

            mAudioRouteOutput = (ViewGroup) mPreviewLayout.findViewById(R.id.rl_audio_route);
            mAudioOutputViewHolderMap = new HashMap<>(MAX_AUDIO_OUTPUT_COUNT);
            for (int i = 0; i < MAX_AUDIO_OUTPUT_COUNT; i++) {
                View routeOutputRoot = mAudioRouteOutput.findViewById(getAudioOutputResID(i));
                String outputIndex = String.format(Locale.ENGLISH, "%02d", Defines.AUDIO_ROUTE_INDEX_OUTPUT_BEGIN + i);
                mAudioOutputViewHolderMap.put(outputIndex, AudioOutputViewHolder.createInstance(routeOutputRoot));
            }

            Button routePreviewApplyButton = (Button) mPreviewLayout.findViewById(R.id.btn_preview_apply);
            routePreviewApplyButton.setOnClickListener(this);
            Button routePreviewCloseButton = (Button) mPreviewLayout.findViewById(R.id.btn_preview_close);
            routePreviewCloseButton.setOnClickListener(this);

            //endregion preview layout

            SharedPreferences prefs = Util.getSharedPreferences(getContext());
            mIsRoutingModeShowStart = prefs.getBoolean(Defines.PREFERENCES_KEY_ROUTE_IS_SHOW, false);

            hidePreview();

            requestRouteMode();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mViewMyself;
    }

    private class RoutingListItemInteractionListener implements RoutingListAdapter.ItemInteractionListener {
        @Override
        public void onApply(final int position) {
            try {
                requestRouteDetail(position, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onPreview(int position) {
            try {
                requestRouteDetail(position, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void requestRouteDetail(final int position, final boolean requestMqtt) {
        try {
            RouteListItemModel routeListItemModel = mRouteListItemModelList.get(position);
            int routeIndex = routeListItemModel.getIndex();

            mRoutePreviewModel.setRouteListItemModel(routeListItemModel);
            mRoutePreviewModel.setRouteDetailModelList(null);

            showMainActiviyLoadingProgressBar();

            RequestParams requestParams = new RequestParams();
            requestParams.put("IOR_Index", routeIndex);
            NetClient.post(getContext(), Defines.URL_GET_ROUTE_DETAIL, requestParams, new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(String jsonResponse) {
                    try {
                        hideMainAcitivyLoadingProgressBar();

                        if (!Util.isStringNullOrEmpty(jsonResponse)) {
                            Gson gson = new Gson();
                            List<RouteDetailModel> routeDetailModelList = gson.fromJson(jsonResponse, new TypeToken<List<RouteDetailModel>>() {
                            }.getType());

                            if (routeDetailModelList != null && !routeDetailModelList.isEmpty()) {
                                mRoutePreviewModel.setRouteDetailModelList(routeDetailModelList);
                            }
                        }

                        if (mRoutePreviewModel.getRouteDetailModelList() != null) {
                            if (requestMqtt) {
                                requestMqttApplyRoute();
                            } else {
                                showPreview();
                            }
                        } else {
                            Util.showAlertDialog(getContext(),
                                getString(R.string.dialog_title),
                                getString(R.string.dialog_message_get_route_detail_fail),
                                getString(R.string.retry),
                                getString(R.string.finish),
                                new AlertDialogButtonCallback() {
                                    @Override
                                    public void onClickButton(boolean isPositiveClick) {
                                        if (isPositiveClick) {
                                            requestRouteDetail(position, requestMqtt);
                                        } else {
                                            getActivity().finish();
                                        }
                                    }
                                });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void requestMqttSetRouteModeToAuto() {
        mOnFragmentInteractionListener.onRequestMqttSetRouteMode("RouteMode|Auto");

        SharedPreferences prefs = Util.getSharedPreferences(getContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(Defines.PREFERENCES_KEY_ROUTE_INDEX, -1);
        editor.apply();

        if (mAppliedRouteListItemModel != null) {
            mAppliedRouteListItemModel.setApplyYN("N");
            mRoutingListRecyclerView.getAdapter().notifyDataSetChanged();
        }
    }

	public void requestMqttSetRouteModeToShowStart() {
		mOnFragmentInteractionListener.onRequestMqttSetRouteMode("RouteMode|Show");

		SharedPreferences prefs = Util.getSharedPreferences(getContext());
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt(Defines.PREFERENCES_KEY_ROUTE_INDEX, -1);
		editor.apply();

		if (mAppliedRouteListItemModel != null) {
			mAppliedRouteListItemModel.setApplyYN("N");
			mRoutingListRecyclerView.getAdapter().notifyDataSetChanged();
		}
	}

    private void requestMqttApplyRoute() {
        try {
            List<RouteDetailModel> routeDetailModelList = mRoutePreviewModel.getRouteDetailModelList();
            if (routeDetailModelList == null) {
                return;
            }

            List<RouteDetailModel> videoRouteList = new LinkedList<>();
            List<RouteDetailModel> audioRouteList = new LinkedList<>();

            int size = routeDetailModelList.size();
            for (int i = 0; i < size; i++) {
                RouteDetailModel model = routeDetailModelList.get(i);
                if (model.getRouteType().equals("VD")) {
                    videoRouteList.add(model);
                } else if (model.getRouteType().equals("AD")) {
                    audioRouteList.add(model);
                }
            }

            if (videoRouteList.isEmpty() || audioRouteList.isEmpty()) {
                return;
            }

            mOnFragmentInteractionListener.onRequestMqttSetRouteMode("RouteMode|Manual");

            StringBuilder builder = new StringBuilder();
            //bulid video route message => "VideoRoute|01,01,02,02..."
            builder.append("VideoRoute|");
            size = videoRouteList.size();
            for (int i = 0; i < size; i++) {
                RouteDetailModel model = videoRouteList.get(i);
                builder.append(String.format("%s,%s", model.getInputCode(), model.getOutputCode()));

                if (i < size - 1) {
                    builder.append(",");
                }
            }

            mOnFragmentInteractionListener.onRequestMqttApplyManualRouting(builder.toString());

            //bulid audio route message => "AudioRoute|01,01,02,02..."
            builder.setLength(0);
            builder.append("AudioRoute|");
            size = audioRouteList.size();
            for (int i = 0; i < size; i++) {
                RouteDetailModel model = audioRouteList.get(i);
                if (model.getDeviceType().equals("MUTE")) {
                    builder.append(String.format(",%s", model.getOutputCode()));
                } else {
                    builder.append(String.format("%s,%s", model.getInputCode(), model.getOutputCode()));
                }

                if (i < size - 1) {
                    builder.append(",");
                }
            }

            mOnFragmentInteractionListener.onRequestMqttApplyManualRouting(builder.toString());

            if(mAppliedRouteListItemModel != null) {
                mAppliedRouteListItemModel.setApplyYN("N");
            }

            mAppliedRouteListItemModel = mRoutePreviewModel.getRouteListItemModel();
            mAppliedRouteListItemModel.setApplyYN("Y");

            mRoutingListRecyclerView.getAdapter().notifyDataSetChanged();

            SharedPreferences prefs = Util.getSharedPreferences(getContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt(Defines.PREFERENCES_KEY_ROUTE_INDEX, mAppliedRouteListItemModel.getIndex());
            editor.apply();
        } catch (Exception e) {
            DebugLog.e(TAG, "requestMqttApplyRoute exception. msg = " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mOnFragmentInteractionListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mOnFragmentInteractionListener = null;
    }

    private void requestRouteMode() {
        showMainActiviyLoadingProgressBar();

        NetClient.post(getContext(), Defines.URL_GET_ROUTE_MODE, null, new NetResponseCallback(new NetResponse() {
            @Override
            public void onResponse(String jsonResponse) {
                try {
                    hideMainAcitivyLoadingProgressBar();

                    boolean isValid = false;
                    if (!Util.isStringNullOrEmpty(jsonResponse)) {
                        Gson gson = new Gson();
                        mRouteModeModel = gson.fromJson(jsonResponse, RouteModeModel.class);
                        if (mRouteModeModel != null) {
                            isValid = true;
                            applyRouteModeToUI();

                            requestRouteList();
                        }
                    }

                    if (!isValid) {
                        Util.showAlertDialog(getContext(),
                            getString(R.string.dialog_title),
                            getString(R.string.dialog_message_get_route_mode_fail),
                            getString(R.string.retry),
                            getString(R.string.finish),
                            new AlertDialogButtonCallback() {
                                @Override
                                public void onClickButton(boolean isPositiveClick) {
                                    if (isPositiveClick) {
                                        requestRouteMode();
                                    } else {
                                        getActivity().finish();
                                    }
                                }
                            });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }));
    }

    private void requestRouteList() {
        showMainActiviyLoadingProgressBar();

        RequestParams requestParams = new RequestParams();
        requestParams.put("CurrentPage", 1);
        requestParams.put("PerPageNum", 10000);

        NetClient.post(getContext(), Defines.URL_GET_ROUTE_LIST, requestParams, new NetResponseCallback(new NetResponse() {
            @Override
            public void onResponse(String jsonResponse) {
                try {
                    hideMainAcitivyLoadingProgressBar();

                    boolean isValid = false;
                    if (!Util.isStringNullOrEmpty(jsonResponse)) {

                        mRouteListItemModelList.clear();
                        mAppliedRouteListItemModel = null;

                        SharedPreferences prefs = Util.getSharedPreferences(getContext());
                        int appliedRouteIndex = prefs.getInt(Defines.PREFERENCES_KEY_ROUTE_INDEX, -1);

                        Gson gson = new Gson();
                        try {
                            List<RouteListItemModel> newList = gson.fromJson(jsonResponse, new TypeToken<List<RouteListItemModel>>() {
                            }.getType());
                            if (newList != null) {
                                for (int i = 0; i < newList.size(); i++) {
                                    RouteListItemModel model = newList.get(i);
                                    mRouteListItemModelList.add(model);

                                    if (appliedRouteIndex != -1 && appliedRouteIndex == model.getIndex()) {
                                        mAppliedRouteListItemModel = model;
                                        model.setApplyYN("Y");
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (appliedRouteIndex != -1 && mAppliedRouteListItemModel == null) {
                            prefs.edit().putInt(Defines.PREFERENCES_KEY_ROUTE_INDEX, -1).apply();
                        }

                        mRoutingListRecyclerView.getAdapter().notifyDataSetChanged();

                        isValid = true;
                    }

                    if (!isValid) {
                        Util.showAlertDialog(getContext(),
                            getString(R.string.dialog_title),
                            getString(R.string.dialog_message_get_route_list_fail),
                            getString(R.string.retry),
                            getString(R.string.finish),
                            new AlertDialogButtonCallback() {
                                @Override
                                public void onClickButton(boolean isPositiveClick) {
                                    if (isPositiveClick) {
                                        requestRouteList();
                                    } else {
                                        getActivity().finish();
                                    }
                                }
                            });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }));
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        try {
            switch (v.getId()) {
                case R.id.btn_routing_mode_auto: {
                    if (mRouteModeModel != null) {
                        boolean isCurAutoMode = mRouteModeModel.getAutoYN().equalsIgnoreCase("Y");
                        if (!isCurAutoMode) {
                            requestMqttSetRouteModeToAuto();
                        }
                    }
                }
                break;
	            case R.id.btn_routing_mode_start: {
		            if(!mIsRoutingModeShowStart) {
                        requestMqttSetRouteModeToShowStart();
		            }
	            }
	            break;
	            case R.id.btn_routing_mode_stop: {
                    if(mIsRoutingModeShowStart) {
                        requestMqttSetRouteModeToAuto();
                    }
	            }
	            break;
                case R.id.btn_video_route: {
                    if (!mPreviewVideoRouteButton.isSelected()) {
                        mPreviewVideoRouteButton.setSelected(true);
                        mPreviewAudioRouteButton.setSelected(false);
                        applyVideoRoutePreviewUI();
                    }
                }
                break;
                case R.id.btn_audio_route: {
                    if (!mPreviewAudioRouteButton.isSelected()) {
                        mPreviewAudioRouteButton.setSelected(true);
                        mPreviewVideoRouteButton.setSelected(false);
                        applyAudioRoutePreviewUI();
                    }
                }
                break;
                case R.id.btn_preview_apply: {
                    try {
                        requestMqttApplyRoute();
                        hidePreview();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
                case R.id.btn_preview_close: {
                    hidePreview();
                }
                break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveRouteModeStartShow(boolean _mIsRoutingModeShowStart) {
        try {
            SharedPreferences prefs = Util.getSharedPreferences(getContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(Defines.PREFERENCES_KEY_ROUTE_IS_SHOW, _mIsRoutingModeShowStart);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void applyRouteModeToUI() {
        try {
	        boolean isAutoMode = true;

            if (mRouteModeModel != null) {
                isAutoMode = mRouteModeModel.getAutoYN().equalsIgnoreCase("Y");
                DebugLog.e(TAG, "cur route mode = " + (isAutoMode ? "Auto" : "Manual"));
            }

            mRoutingModeAutoButton.setSelected(isAutoMode);
	        mRoutingModeManualButton.setSelected(!isAutoMode);

            mRoutingModeShowStartButton.setSelected(mIsRoutingModeShowStart);
            mRoutingModeShowStopButton.setSelected(!mIsRoutingModeShowStart);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void changeRouteMode(String modeData) {
        try {
            if (mRouteModeModel != null) {
                mRouteModeModel.setAutoYN(modeData.equals("Auto") ? "Y" : "N");
            } else {
                DebugLog.e(TAG, "changeRouteMode - route mode model is null. modeData = " + modeData);
            }

            if(modeData.equals("Auto")) {
                if(mIsRoutingModeShowStart) {
                    mIsRoutingModeShowStart = !mIsRoutingModeShowStart;
                    saveRouteModeStartShow(mIsRoutingModeShowStart);
                }
            } else if(modeData.equals("Show")) {
                if(!mIsRoutingModeShowStart) {
                    mIsRoutingModeShowStart = !mIsRoutingModeShowStart;
                    saveRouteModeStartShow(mIsRoutingModeShowStart);
                }
            }

	        applyRouteModeToUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showPreview() {
        try {
            if (mRoutePreviewModel.getRouteListItemModel() == null || mRoutePreviewModel.getRouteDetailModelList() == null) {
                return;
            }

            showBackground();

            mPreviewLayout.setVisibility(View.VISIBLE);

            mPreviewRouteName.setText(mRoutePreviewModel.getRouteListItemModel().getName());

            mPreviewVideoRouteButton.setSelected(true);
            mPreviewAudioRouteButton.setSelected(false);

            applyVideoRoutePreviewUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hidePreview() {
        mPreviewLayout.setVisibility(View.GONE);
        hideBackground();
    }

    private void applyVideoRoutePreviewUI() {
        try {
            List<RouteDetailModel> routeDetailModelList = mRoutePreviewModel.getRouteDetailModelList();
            if (routeDetailModelList == null) {
                return;
            }

            List<RouteDetailModel> videoRouteList = new LinkedList<>();

            int size = routeDetailModelList.size();
            for (int i = 0; i < size; i++) {
                RouteDetailModel model = routeDetailModelList.get(i);
                if (model.getRouteType().equals("VD")) {
                    videoRouteList.add(model);
                }
            }

            if (videoRouteList.isEmpty()) {
                return;
            }

            mVideoRouteOutput.setVisibility(View.VISIBLE);
            mAudioRouteOutput.setVisibility(View.GONE);

            size = videoRouteList.size();
            for (int i = 0; i < MAX_VIDEO_OUTPUT_COUNT; i++) {
                if (i < size) {
                    RouteDetailModel model = videoRouteList.get(i);
                    RouteOutputViewHolder viewHolder = mVideoOutputViewHolderMap.get(model.getIndexOutput());
                    if (viewHolder != null) {
                        viewHolder.bindData(model.getDeviceName(), getDeviceIconResID(model.getDeviceType()));
                    }
                } else {
                    DebugLog.e(TAG, "video output data mismatch");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void applyAudioRoutePreviewUI() {
        try {
            List<RouteDetailModel> routeDetailModelList = mRoutePreviewModel.getRouteDetailModelList();
            if (routeDetailModelList == null) {
                return;
            }

            List<RouteDetailModel> audioRouteList = new LinkedList<>();

            int size = routeDetailModelList.size();
            for (int i = 0; i < size; i++) {
                RouteDetailModel model = routeDetailModelList.get(i);
                if (model.getRouteType().equals("AD")) {
                    audioRouteList.add(model);
                }
            }

            if (audioRouteList.isEmpty()) {
                return;
            }

            mAudioRouteOutput.setVisibility(View.VISIBLE);
            mVideoRouteOutput.setVisibility(View.GONE);

            size = audioRouteList.size();
            for (int i = 0; i < MAX_AUDIO_OUTPUT_COUNT; i++) {
                if (i < size) {
                    RouteDetailModel model = audioRouteList.get(i);
                    AudioOutputViewHolder viewHolder = mAudioOutputViewHolderMap.get(model.getIndexOutput());
                    viewHolder.bindData(getZoneName(i), model.getDeviceName(), getDeviceIconResID(model.getDeviceType()));
                } else {
                    DebugLog.e(TAG, "audio output data mismatch");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getVideoOutputResID(int index) {
        switch (index) {
            case 0:
                return R.id.rl_route_output_video_1;
            case 1:
                return R.id.rl_route_output_video_2;
            case 2:
                return R.id.rl_route_output_video_3;
            case 3:
                return R.id.rl_route_output_video_4;
            case 4:
                return R.id.rl_route_output_video_5;
            case 5:
                return R.id.rl_route_output_video_6;
            case 6:
                return R.id.rl_route_output_video_7;
            case 7:
                return R.id.rl_route_output_video_8;
            case 8:
                return R.id.rl_route_output_video_9;
            case 9:
                return R.id.rl_route_output_video_10;
            case 10:
                return R.id.rl_route_output_video_11;
            case 11:
                return R.id.rl_route_output_video_12;
            case 12:
                return R.id.rl_route_output_video_13;
            case 13:
                return R.id.rl_route_output_video_14;
            case 14:
                return R.id.rl_route_output_video_15;
            case 15:
                return R.id.rl_route_output_video_16;
            case 16:
                return R.id.rl_route_output_video_17;
            case 17:
                return R.id.rl_route_output_video_18;
            case 18:
                return R.id.rl_route_output_video_19;
            case 19:
                return R.id.rl_route_output_video_20;
            case 20:
                return R.id.rl_route_output_video_21;
            case 21:
                return R.id.rl_route_output_video_22;
            default:
                return -1;
        }
    }

    private int getAudioOutputResID(int index) {
        switch (index) {
            case 0:
                return R.id.rl_route_output_audio_1;
            case 1:
                return R.id.rl_route_output_audio_2;
            case 2:
                return R.id.rl_route_output_audio_3;
            case 3:
                return R.id.rl_route_output_audio_4;
            default:
                return -1;
        }
    }

    private int getDeviceIconResID(String deviceType) {
        switch (deviceType) {
            case "PC":
                return R.drawable.icon_pc;
            case "CAM":
                return R.drawable.icon_cam;
            case "ENG":
                return R.drawable.icon_eng_cam;
            case "MIC":
                return R.drawable.icon_mic;
            case "SPK":
                return R.drawable.icon_speaker;
            case "MUTE":
                return R.drawable.icon_mute;
            default:
                return -1;
        }
    }

    private String getZoneName(int index) {
        switch (index) {
            case 0:
                return "A Zone";
            case 1:
                return "B Zone";
            case 2:
                return "C Zone\n#1";
            case 3:
                return "C Zone\n#2";
            default:
                return "";
        }
    }
}