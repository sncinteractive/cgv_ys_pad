package com.cgv.ys.manager.pad.cgv_ys.view.ViewHolder;

import android.view.View;
import android.widget.TextView;

import com.cgv.ys.manager.pad.cgv_ys.R;

/**
 * Created by eklee on 2017. 6. 19..
 */

public class AudioOutputViewHolder {
    TextView mZoneNameTextView;
    RouteOutputViewHolder mRouteOutputViewHolder;

    public AudioOutputViewHolder() {
    }

    public static AudioOutputViewHolder createInstance(View view) {
        AudioOutputViewHolder instance = new AudioOutputViewHolder();
        instance.mZoneNameTextView = (TextView) view.findViewById(R.id.tv_audio_zone_name);
        instance.mRouteOutputViewHolder = RouteOutputViewHolder.createInstance(view.findViewById(R.id.rl_route_output));

        return instance;
    }

    public void bindData(String zoneName, String deviceName, int iconResID) {
        mZoneNameTextView.setText(zoneName);
        mRouteOutputViewHolder.bindData(deviceName, iconResID);
    }
}
