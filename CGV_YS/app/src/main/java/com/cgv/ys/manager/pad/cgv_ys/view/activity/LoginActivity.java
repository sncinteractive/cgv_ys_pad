package com.cgv.ys.manager.pad.cgv_ys.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.cgv.ys.manager.pad.cgv_ys.R;
import com.cgv.ys.manager.pad.cgv_ys.common.AlertDialogButtonCallback;
import com.cgv.ys.manager.pad.cgv_ys.common.DebugLog;
import com.cgv.ys.manager.pad.cgv_ys.common.Defines;
import com.cgv.ys.manager.pad.cgv_ys.common.Util;
import com.cgv.ys.manager.pad.cgv_ys.model.UserInfoModel;
import com.cgv.ys.manager.pad.cgv_ys.network.NetClient;
import com.cgv.ys.manager.pad.cgv_ys.network.NetResponse;
import com.cgv.ys.manager.pad.cgv_ys.network.NetResponseCallback;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "LoginActivity";

    private RelativeLayout mLoadingProgressBar;

    private EditText mIdEditText;
    private EditText mPwEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login);

            mLoadingProgressBar = (RelativeLayout) findViewById(R.id.loading_progress);

            mIdEditText = (EditText) findViewById(R.id.et_id);
            mIdEditText.setText("");
            mPwEditText = (EditText) findViewById(R.id.et_pw);
            mPwEditText.setText("");

            ApplicationInfo applicationInfo = getApplicationInfo();
            boolean appDebuggable = (applicationInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
            if (appDebuggable) {
                mIdEditText.setText("yspaduser");
                mPwEditText.setText("Tlwlqmdl1!");
            }

            Button loginButton = (Button) findViewById(R.id.btn_login);
            loginButton.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login: {
                requestLogin();
            }
            break;
        }
    }

    @Override
    public void onBackPressed() {
        Util.showAlertDialog(this,
            getString(R.string.dialog_title),
            getString(R.string.dialog_message_finish_app),
            getString(R.string.finish),
            getString(R.string.cancel),
            new AlertDialogButtonCallback() {
                @Override
                public void onClickButton(boolean isPositiveClick) {
                    if (isPositiveClick) {
                        finish();
                    }
                }
            });
    }

    private void requestLogin() {
        try {
            String id = mIdEditText.getText().toString();
            if (id.isEmpty()) {
                return;
            }

            String pw = mPwEditText.getText().toString();
            if (pw.isEmpty()) {
                return;
            }

            showLoadingProgressBar();

            RequestParams requestParams = new RequestParams();
            requestParams.put("MR_ID", id);
            requestParams.put("MR_PWD", pw);

            NetClient.post(this, Defines.URL_LOGIN, requestParams, new NetResponseCallback(new NetResponse() {
                @Override
                public void onResponse(String jsonResponse) {
                    hideLoadingProgressBar();

                    boolean isValid = false;
                    if (!Util.isStringNullOrEmpty(jsonResponse)) {
                        Gson gson = new Gson();
                        UserInfoModel userInfoModel = gson.fromJson(jsonResponse, UserInfoModel.class);
                        if (userInfoModel != null && userInfoModel.getUserID() != null) {
                            isValid = true;

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            intent.putExtra("userInfo", userInfoModel);
                            setResult(Activity.RESULT_OK, intent);
                            finish();
                        }
                    }

                    if (!isValid) {
                        Util.showAlertDialog(LoginActivity.this,
                            getString(R.string.dialog_title),
                            getString(R.string.dialog_message_mismatch_id_or_pw),
                            getString(R.string.confirm),
                            null,
                            null
                        );
                    }
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showLoadingProgressBar() {
        mLoadingProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoadingProgressBar() {
        mLoadingProgressBar.setVisibility(View.GONE);
    }
}
