package com.cgv.ys.manager.pad.cgv_ys.common;

/**
 * Created by eklee on 2017. 6. 14..
 */

public class Defines {

    public static final String PUBLISH_TOPIC = "YSSERVER";
    public static final String SUBSCRIBE_TOPIC = "YSCLIENT";

    //preferences
    public static final String SHARED_PREFERENCES_NAME = "sharedPrefs";
    public static final String PREFERENCES_KEY_USER_ID = "UserId";
    public static final String PREFERENCES_KEY_USER_PW = "UserPw";
    public static final String PREFERENCES_KEY_USER_NAME = "UserName";

    public static final String PREFERENCES_KEY_ROUTE_INDEX = "RouteIndex";
    public static final String PREFERENCES_KEY_ROUTE_IS_SHOW = "RouteIsShow";
    public static final String PREFERENCES_KEY_LIVE_SCHEDULE_INDEX = "LiveScheduleIndex";
    public static final String PREFERENCES_KEY_LIVE_SCHEDULE_SCENE_INDEX = "LiveScheduleSceneIndex";

    //url
    public static final String URL_LOGIN = "/Api/YoungSan/Login";
    public static final String URL_GET_ROUTE_MODE = "/Api/YoungSan/RouteMode";
    public static final String URL_SET_ROUTE_MODE = "/Api/YoungSan/SetRouteMode";
    public static final String URL_GET_ROUTE_LIST = "/Api/YoungSan/GetRouteList";
    public static final String URL_GET_ROUTE_DETAIL = "/Api/YoungSan/GetRouteDetail";
    public static final String URL_GET_LIVE_SCHEDULE_LIST = "/Api/YoungSan/GetScheduleList";
    public static final String URL_GET_LIVE_SCHEDULE_DETAIL = "/Api/YoungSan/GetScheduleDetail";

    //request code
    public static final int REQUEST_CODE_LOGIN = 1;

    // tilt, pan, zoom min max value
    public static final int TILT_MIN_VALUE = -175;
    public static final int TILT_MAX_VALUE = 175;
    public static final int PAN_MIN_VALUE = -90;
    public static final int PAN_MAX_VALUE = 30;
    public static final int ZOOM_MIN_VALUE = 12;
    public static final int ZOOM_MAX_VALUE = 120;

    // route index output
    public static final int VIDEO_ROUTE_INDEX_OUTPUT_BEGIN = 35;
    public static final int AUDIO_ROUTE_INDEX_OUTPUT_BEGIN = 59;
}
