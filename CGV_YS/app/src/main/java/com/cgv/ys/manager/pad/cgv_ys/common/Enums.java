package com.cgv.ys.manager.pad.cgv_ys.common;

/**
 * Created by eklee on 2017. 6. 14..
 */

public class Enums {
    public enum CommandType {
        CAM("Cam"),
        VIDEO_ROUTE("VideoRoute"),
        AUDIO_ROUTE("AudioRoute");

        CommandType(String value) {
            mValue = value;
        }

        public String getValue() {
            return mValue;
        }

        private String mValue;
    }

    public enum CommandOption {
        PAN("Pan"),
        TILT("Tilt"),
        ZOOM("Zoom");

        CommandOption(String value) {
            mValue = value;
        }

        public String getValue() {
            return mValue;
        }

        private String mValue;
    }

    public enum CommandAction {
        UP("Plus"),
        DOWN("Minus"),
        SET("Set");

        CommandAction(String value) {
            mValue = value;
        }

        public String getValue() {
            return mValue;
        }

        private String mValue;
    }

    public enum TabType {
        CAMERA_CONTROL(1),
        LIVE_SCHEDULE(2),
        INPUT_SOURCE_ROUTING(3);

        TabType(int value) {
            mValue = value;
        }

        public int getValue() {
            return mValue;
        }

        private int mValue;

    }
}
