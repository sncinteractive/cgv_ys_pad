package com.cgv.ys.manager.pad.cgv_ys.common;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by eklee on 2017. 11. 2..
 */

public class DownloadImageTask extends AsyncTask<String, Integer, Bitmap> {
    private static final String TAG = "DownloadImageTask";

    private ImageView mImageView;

    public DownloadImageTask(ImageView imageView) {
        mImageView = imageView;
    }

    @Override
    protected Bitmap doInBackground(String... strings) {
        Bitmap bitmap = null;
        HttpURLConnection connection = null;
        BufferedInputStream bis = null;
        try {
            URL url = new URL(strings[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(30 * 1000);
            connection.setReadTimeout(30 * 1000);
            connection.setRequestProperty("resolution", "640");
            connection.setRequestProperty("qualiity", "1");
            connection.setRequestProperty("page", String.valueOf(System.currentTimeMillis()));
            connection.connect();

            bis = new BufferedInputStream(connection.getInputStream());
            bitmap = BitmapFactory.decodeStream(bis);
        } catch (Exception e) {
            e.printStackTrace();
            DebugLog.e(TAG, "doInBackground exception. " + e.toString());
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (connection != null) {
                connection.disconnect();
            }
        }

        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        try {
            if (mImageView != null && mImageView.getVisibility() == View.VISIBLE) {
                mImageView.setImageBitmap(bitmap);
            }
        } catch (Exception e) {
            e.printStackTrace();
            DebugLog.e(TAG, "onPostExecute exception. " + e.toString());
        }
    }

    @Override
    protected void onCancelled(Bitmap bitmap) {
        DebugLog.d(TAG, "onCancelled");
    }
}
