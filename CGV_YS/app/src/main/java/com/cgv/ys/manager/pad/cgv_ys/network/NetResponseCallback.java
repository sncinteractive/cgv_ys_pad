package com.cgv.ys.manager.pad.cgv_ys.network;

/**
 * Created by eklee on 2017. 6. 13..
 */

public class NetResponseCallback {
    private NetResponse mNetResponse;

    public NetResponseCallback(NetResponse mNetResponse) {
        this.mNetResponse = mNetResponse;
    }

    public void onResponse(String jsonResponse) {
        this.mNetResponse.onResponse(jsonResponse);
    }
}