package com.cgv.ys.manager.pad.cgv_ys.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by eklee on 2017. 6. 19..
 */

public class LiveScheduleListItemModel {
    @SerializedName("YSH_Index")
    private int mIndex;

    @SerializedName("YSH_Name")
    private String mName;

    private boolean mIsApplied;

    public LiveScheduleListItemModel() {
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public boolean isApplied() {
        return mIsApplied;
    }

    public void setApplied(boolean applied) {
        mIsApplied = applied;
    }
}
