package com.cgv.ys.manager.pad.cgv_ys.view.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cgv.ys.manager.pad.cgv_ys.R;

/**
 * Created by eklee on 2017. 6. 19..
 */

public class VideoLayerViewHolder {
    private TextView mNameTextView;
    private ImageView mIconImageView;

    public VideoLayerViewHolder() {
    }

    public static VideoLayerViewHolder createInstance(View view) {
        VideoLayerViewHolder instance = new VideoLayerViewHolder();
        instance.mNameTextView = (TextView) view.findViewById(R.id.tv_video_name);
        instance.mIconImageView = (ImageView) view.findViewById(R.id.iv_video_icon);
        return instance;
    }

    public void bindData(String inputName, int iconResID) {
        mNameTextView.setText(inputName);
        mIconImageView.setImageResource(iconResID);
    }
}
