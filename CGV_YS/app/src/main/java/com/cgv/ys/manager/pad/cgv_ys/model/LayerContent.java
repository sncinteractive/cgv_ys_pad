package com.cgv.ys.manager.pad.cgv_ys.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eklee on 2017. 6. 20..
 */

public class LayerContent {
    @SerializedName("SLC_Type")
    private String mLayerContentType; // "CM(camera), PL(playlist), MC(mic), MT(mute), SP(speaker)"

    @SerializedName("SLC_IOSInputName")
    private String mInputName;

    @SerializedName("SLC_RefName")
    private String mRefName;

    public LayerContent() {
    }

    public LayerContent(String layerContentType, String inputName, String refName) {
        mLayerContentType = layerContentType;
        mInputName = inputName;
        mRefName = refName;
    }

    public String getLayerContentType() {
        return mLayerContentType;
    }

    public void setLayerContentType(String layerContentType) {
        mLayerContentType = layerContentType;
    }

    public String getInputName() {
        return mInputName;
    }

    public void setInputName(String inputName) {
        mInputName = inputName;
    }

    public String getRefName() {
        return mRefName;
    }

    public void setRefName(String refName) {
        mRefName = refName;
    }
}
