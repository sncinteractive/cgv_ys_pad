package com.cgv.ys.manager.pad.cgv_ys.view.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cgv.ys.manager.pad.cgv_ys.R;

/**
 * Created by eklee on 2017. 6. 19..
 */

public class RouteOutputViewHolder {
    private TextView mDeviceNameTextView;
    private ImageView mIconImageView;

    public RouteOutputViewHolder() {
    }

    public static RouteOutputViewHolder createInstance(View view) {
        RouteOutputViewHolder instance = new RouteOutputViewHolder();
        instance.mDeviceNameTextView = (TextView) view.findViewById(R.id.tv_route_output_name);
        instance.mIconImageView = (ImageView) view.findViewById(R.id.iv_route_output_icon);
        return instance;
    }

    public void bindData(String deviceName, int iconResID) {
        mDeviceNameTextView.setText(deviceName);
        mIconImageView.setImageResource(iconResID);
    }
}
