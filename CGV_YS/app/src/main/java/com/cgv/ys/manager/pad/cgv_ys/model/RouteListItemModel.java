package com.cgv.ys.manager.pad.cgv_ys.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by eklee on 2017. 6. 16..
 */

public class RouteListItemModel {
    @SerializedName("IOR_Index")
    private int mIndex;

    @SerializedName("IOR_Name")
    private String mName;

    @SerializedName("IOR_ApplyYN")
    private String mApplyYN;

    private List<RouteDetailModel> mRouteDetailModelList;

    public RouteListItemModel() {
    }

    public RouteListItemModel(int index, String name, String applyYN, List<RouteDetailModel> routeDetailModelList) {
        mIndex = index;
        mName = name;
        mApplyYN = applyYN;
        mRouteDetailModelList = routeDetailModelList;
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getApplyYN() {
        return mApplyYN;
    }

    public void setApplyYN(String applyYN) {
        mApplyYN = applyYN;
    }

    public List<RouteDetailModel> getRouteDetailModelList() {
        return mRouteDetailModelList;
    }

    public void setRouteDetailModelList(List<RouteDetailModel> routeDetailModelList) {
        mRouteDetailModelList = routeDetailModelList;
    }
}
