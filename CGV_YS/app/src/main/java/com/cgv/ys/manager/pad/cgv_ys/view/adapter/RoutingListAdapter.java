package com.cgv.ys.manager.pad.cgv_ys.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.cgv.ys.manager.pad.cgv_ys.R;
import com.cgv.ys.manager.pad.cgv_ys.common.DebugLog;
import com.cgv.ys.manager.pad.cgv_ys.model.RouteListItemModel;

import java.util.List;

/**
 * Created by eklee on 2017. 6. 16..
 */

public class RoutingListAdapter extends RecyclerView.Adapter<RoutingListAdapter.ViewHolder> {
    public interface ItemInteractionListener {
        void onApply(int position);

        void onPreview(int position);
    }

    private List<RouteListItemModel> mRouteListItemModelList;

    private ItemInteractionListener mItemInteractionListener;
    private final RoutingListAdapter.ViewHolder.ViewHolderInteractionListener mViewHolderInteractionListener = new ViewHolderInteractionListenerImpl();

    public RoutingListAdapter(List<RouteListItemModel> titleList, ItemInteractionListener listener) {
        mRouteListItemModelList = titleList;
        mItemInteractionListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_routing_list, parent, false);
        return new ViewHolder(view, mViewHolderInteractionListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            RouteListItemModel model = mRouteListItemModelList.get(position);
            if (model != null) {
                holder.mTitleTextView.setText(model.getName());

                boolean isApplied = model.getApplyYN().equalsIgnoreCase("Y");
                holder.mTitleTextView.setSelected(isApplied);
                holder.mFrameView.setSelected(isApplied);
                holder.mPreviewButton.setSelected(isApplied);
                holder.mApplyButton.setSelected(isApplied);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mRouteListItemModelList.size();
    }

    public class ViewHolderInteractionListenerImpl implements ViewHolder.ViewHolderInteractionListener {

        @Override
        public void onApply(int position) {
            mItemInteractionListener.onApply(position);
        }

        @Override
        public void onPreview(int position) {
            mItemInteractionListener.onPreview(position);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public interface ViewHolderInteractionListener {
            void onApply(int position);

            void onPreview(int position);
        }

        public TextView mTitleTextView;
        private ViewHolderInteractionListener mInteractionListener;

        private View mFrameView;
        private Button mPreviewButton;
        private Button mApplyButton;

        public ViewHolder(View itemView, ViewHolderInteractionListener listener) {
            super(itemView);

            mTitleTextView = (TextView) itemView.findViewById(R.id.tv_title);

            mFrameView = itemView.findViewById(R.id.ll_route_frame);
            mPreviewButton = (Button) itemView.findViewById(R.id.btn_preview);
            mPreviewButton.setOnClickListener(this);
            mApplyButton = (Button) itemView.findViewById(R.id.btn_apply);
            mApplyButton.setOnClickListener(this);

            mInteractionListener = listener;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_preview: {
                    if (mInteractionListener != null) {
                        mInteractionListener.onPreview(getLayoutPosition());
                    }
                }
                break;
                case R.id.btn_apply: {
                    if (mInteractionListener != null) {
                        mInteractionListener.onApply(getLayoutPosition());
                    }
                }
                break;
                default:
                    break;
            }
        }
    }
}
