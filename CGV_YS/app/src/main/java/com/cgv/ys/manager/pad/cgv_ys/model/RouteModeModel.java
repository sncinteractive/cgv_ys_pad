package com.cgv.ys.manager.pad.cgv_ys.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eklee on 2017. 6. 16..
 */

public class RouteModeModel {
    @SerializedName("IOM_Index")
    private int mIndex;
    @SerializedName("IOM_AutoYN")
    private String mAutoYN;

    public RouteModeModel() {
    }

    public RouteModeModel(int index, String autoYN) {
        mIndex = index;
        mAutoYN = autoYN;
    }

    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int index) {
        mIndex = index;
    }

    public String getAutoYN() {
        return mAutoYN;
    }

    public void setAutoYN(String autoYN) {
        mAutoYN = autoYN;
    }
}
