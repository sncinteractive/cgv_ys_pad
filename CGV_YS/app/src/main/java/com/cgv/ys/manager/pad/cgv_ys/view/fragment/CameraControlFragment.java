package com.cgv.ys.manager.pad.cgv_ys.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.cgv.ys.manager.pad.cgv_ys.R;
import com.cgv.ys.manager.pad.cgv_ys.common.Defines;
import com.cgv.ys.manager.pad.cgv_ys.common.DownloadImageTask;
import com.cgv.ys.manager.pad.cgv_ys.common.Enums;
import com.cgv.ys.manager.pad.cgv_ys.common.Util;
import com.cgv.ys.manager.pad.cgv_ys.model.CameraInfoModel;

public class CameraControlFragment extends FragmentBase implements View.OnClickListener {
    public interface OnFragmentInteractionListener {
        void onRequestMqttCameraStatus(int cameraIndex);

        void onRequestMqttCameraUpDown(int cameraIndex, Enums.CommandOption commandOption, Enums.CommandAction commandAction);

        void onRequestMqttCameraSet(int cameraIndex, Enums.CommandOption commandOption, int value);
    }

    private static final int MAX_CAMERA_COUNT = 3;
    private static final String TAG = "CameraControlFragment";
    public static final String[] CAM_URL_ARRAY = new String[]{
        "http://61.73.59.92:20110/cgi-bin/mjpeg?resolution=640x360&framerate=30&quality=1",
        "http://61.73.59.92:20111/cgi-bin/mjpeg?resolution=640x360&framerate=30&quality=1",
        "http://61.73.59.92:20112/cgi-bin/mjpeg?resolution=640x360&framerate=30&quality=1"
    };

    private OnFragmentInteractionListener mOnFragmentInteractionListener;

    private View mViewMyself;

    private int mSelectedCamIndex;

    private View mLiveCamRootView1;
    private View mLiveCamRootView2;
    private View mLiveCamRootView3;

    private ImageView mCamImageView1;
    private ImageView mCamImageView2;
    private ImageView mCamImageView3;

    private EditText mTiltEditText;
    private EditText mPanEditText;
    private EditText mZoomEditText;

    private SeekBar mTiltSeekBar;
    private SeekBar mPanSeekBar;
    private SeekBar mZoomSeekBar;

    private CameraInfoModel mCameraInfoModel;

    private View mCameraControlView;

    private boolean mIsPaused;

    private Handler mHandler;

    public CameraControlFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {
            mViewMyself = inflater.inflate(R.layout.fragment_camera_control, container, false);

            super.initializeView(mViewMyself);

            setSelectedTabButton(Enums.TabType.CAMERA_CONTROL);

            mLiveCamRootView1 = mViewMyself.findViewById(R.id.live_cam_root_view_1);
            mLiveCamRootView1.setOnClickListener(this);
            mLiveCamRootView2 = mViewMyself.findViewById(R.id.live_cam_root_view_2);
            mLiveCamRootView2.setOnClickListener(this);
            mLiveCamRootView3 = mViewMyself.findViewById(R.id.live_cam_root_view_3);
            mLiveCamRootView3.setOnClickListener(this);

            mCamImageView1 = (ImageView) mViewMyself.findViewById(R.id.live_cam_image_view_1);
            mCamImageView2 = (ImageView) mViewMyself.findViewById(R.id.live_cam_image_view_2);
            mCamImageView3 = (ImageView) mViewMyself.findViewById(R.id.live_cam_image_view_3);

            ImageButton panUpButton = (ImageButton) mViewMyself.findViewById(R.id.ib_pan_up);
            panUpButton.setOnClickListener(this);
            ImageButton panDownButton = (ImageButton) mViewMyself.findViewById(R.id.ib_pan_down);
            panDownButton.setOnClickListener(this);

            ImageButton tiltUpButton = (ImageButton) mViewMyself.findViewById(R.id.ib_tilt_up);
            tiltUpButton.setOnClickListener(this);
            ImageButton tiltDownButton = (ImageButton) mViewMyself.findViewById(R.id.ib_tilt_down);
            tiltDownButton.setOnClickListener(this);

            ImageButton zoomInButton = (ImageButton) mViewMyself.findViewById(R.id.ib_zoom_in);
            zoomInButton.setOnClickListener(this);
            ImageButton zoomOutButton = (ImageButton) mViewMyself.findViewById(R.id.ib_zoom_out);
            zoomOutButton.setOnClickListener(this);

            Button applyTiltButton = (Button) mViewMyself.findViewById(R.id.btn_apply_tilt);
            applyTiltButton.setOnClickListener(this);
            Button applyPanButton = (Button) mViewMyself.findViewById(R.id.btn_apply_pan);
            applyPanButton.setOnClickListener(this);
            Button applyZoomButton = (Button) mViewMyself.findViewById(R.id.btn_apply_zoom);
            applyZoomButton.setOnClickListener(this);

            mTiltEditText = (EditText) mViewMyself.findViewById(R.id.et_tilt);
            mTiltEditText.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        if (keyCode == KeyEvent.KEYCODE_ENTER) {
                            onClickApply(Enums.CommandOption.TILT);

                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            return true;
                        }
                    }
                    return false;
                }
            });
            mPanEditText = (EditText) mViewMyself.findViewById(R.id.et_pan);
            mPanEditText.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        if (keyCode == KeyEvent.KEYCODE_ENTER) {
                            onClickApply(Enums.CommandOption.PAN);

                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            return true;
                        }
                    }
                    return false;
                }
            });
            mZoomEditText = (EditText) mViewMyself.findViewById(R.id.et_zoom);
            mZoomEditText.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        if (keyCode == KeyEvent.KEYCODE_ENTER) {
                            onClickApply(Enums.CommandOption.ZOOM);

                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            return true;
                        }
                    }
                    return false;
                }
            });

            mTiltSeekBar = (SeekBar) mViewMyself.findViewById(R.id.sb_tilt);
            mTiltSeekBar.setMax(Math.abs(Defines.TILT_MIN_VALUE) + Math.abs(Defines.TILT_MAX_VALUE));
            mTiltSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    int value = getValidCommandValue(Enums.CommandOption.TILT, progress + Defines.TILT_MIN_VALUE);
                    mTiltEditText.setText(Integer.toString(value));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    onClickApply(Enums.CommandOption.TILT);
                }
            });

            mPanSeekBar = (SeekBar) mViewMyself.findViewById(R.id.sb_pan);
            mPanSeekBar.setMax(Math.abs(Defines.PAN_MIN_VALUE) + Math.abs(Defines.PAN_MAX_VALUE));
            mPanSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    int value = getValidCommandValue(Enums.CommandOption.PAN, progress + Defines.PAN_MIN_VALUE);
                    mPanEditText.setText(Integer.toString(value));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    onClickApply(Enums.CommandOption.PAN);
                }
            });

            mZoomSeekBar = (SeekBar) mViewMyself.findViewById(R.id.sb_zoom);
            mZoomSeekBar.setMax(Math.abs(Defines.ZOOM_MAX_VALUE) - Math.abs(Defines.ZOOM_MIN_VALUE));
            mZoomSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    int value = getValidCommandValue(Enums.CommandOption.ZOOM, progress + Defines.ZOOM_MIN_VALUE);
                    mZoomEditText.setText(Integer.toString(value));
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    onClickApply(Enums.CommandOption.ZOOM);
                }
            });

            mCameraControlView = mViewMyself.findViewById(R.id.ll_camera_control);

            // 아래 두 줄 변경하면 안됨
            mSelectedCamIndex = -1;
            setSelectedCamIndex(0);

            mIsPaused = false;

            mHandler = new Handler();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mViewMyself;
    }

    @Override
    public void onPause() {
        try {
            super.onPause();

            mIsPaused = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        try {
            super.onResume();

            mIsPaused = false;

            mCameraControlView.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mOnFragmentInteractionListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mCameraControlView.requestFocus();

        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mOnFragmentInteractionListener = null;
    }

    public void applyCameraInfoToModel(Enums.CommandOption commandOption, int value) {
        try {
            if (mCameraInfoModel == null) {
                return;
            }

            switch (commandOption) {
                case TILT: {
                    mCameraInfoModel.setTilt(value);
                }
                break;
                case PAN: {
                    mCameraInfoModel.setPan(value);
                }
                break;
                case ZOOM: {
                    mCameraInfoModel.setZoom(value);
                }
                break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void applyCameraInfoToUI() {
        try {
            if (mCameraInfoModel != null) {
                mTiltEditText.setText(String.valueOf(mCameraInfoModel.getTilt()));
                mPanEditText.setText(String.valueOf(mCameraInfoModel.getPan()));
                mZoomEditText.setText(String.valueOf(mCameraInfoModel.getZoom()));

                mTiltSeekBar.setProgress(mCameraInfoModel.getTilt() + Math.abs(Defines.TILT_MIN_VALUE));
                mPanSeekBar.setProgress(mCameraInfoModel.getPan() + Math.abs(Defines.PAN_MIN_VALUE));
                mZoomSeekBar.setProgress(mCameraInfoModel.getZoom() - Math.abs(Defines.ZOOM_MIN_VALUE));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        try {
            switch (v.getId()) {
                case R.id.live_cam_root_view_1: {
                    setSelectedCamIndex(0);
                    toggleLiveCamStreaming(0);
                }
                break;
                case R.id.live_cam_root_view_2: {
                    setSelectedCamIndex(1);
                    toggleLiveCamStreaming(1);
                }
                break;
                case R.id.live_cam_root_view_3: {
                    setSelectedCamIndex(2);
                    toggleLiveCamStreaming(2);
                }
                break;
                case R.id.ib_pan_up: {
                    mOnFragmentInteractionListener.onRequestMqttCameraUpDown(mSelectedCamIndex, Enums.CommandOption.PAN, Enums.CommandAction.UP);
                }
                break;
                case R.id.ib_pan_down: {
                    mOnFragmentInteractionListener.onRequestMqttCameraUpDown(mSelectedCamIndex, Enums.CommandOption.PAN, Enums.CommandAction.DOWN);
                }
                break;
                case R.id.ib_tilt_up: {
                    mOnFragmentInteractionListener.onRequestMqttCameraUpDown(mSelectedCamIndex, Enums.CommandOption.TILT, Enums.CommandAction.UP);
                }
                break;
                case R.id.ib_tilt_down: {
                    mOnFragmentInteractionListener.onRequestMqttCameraUpDown(mSelectedCamIndex, Enums.CommandOption.TILT, Enums.CommandAction.DOWN);
                }
                break;
                case R.id.ib_zoom_in: {
                    mOnFragmentInteractionListener.onRequestMqttCameraUpDown(mSelectedCamIndex, Enums.CommandOption.ZOOM, Enums.CommandAction.UP);
                }
                break;
                case R.id.ib_zoom_out: {
                    mOnFragmentInteractionListener.onRequestMqttCameraUpDown(mSelectedCamIndex, Enums.CommandOption.ZOOM, Enums.CommandAction.DOWN);
                }
                break;
                case R.id.btn_apply_pan: {
                    onClickApply(Enums.CommandOption.PAN);
                }
                break;
                case R.id.btn_apply_tilt: {
                    onClickApply(Enums.CommandOption.TILT);
                }
                break;
                case R.id.btn_apply_zoom: {
                    onClickApply(Enums.CommandOption.ZOOM);
                }
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onClickApply(Enums.CommandOption commandOption) {
        try {
            String valueString = null;
            switch (commandOption) {
                case PAN: {
                    valueString = mPanEditText.getText().toString();
                }
                break;
                case TILT: {
                    valueString = mTiltEditText.getText().toString();
                }
                break;
                case ZOOM: {
                    valueString = mZoomEditText.getText().toString();
                }
                break;
                default:
                    break;
            }

            if (Util.isStringNullOrEmpty(valueString)) {
                return;
            }

            int value = getValidCommandValue(commandOption, Integer.parseInt(valueString));

            mOnFragmentInteractionListener.onRequestMqttCameraSet(mSelectedCamIndex, commandOption, value);

            //applyCameraInfoToUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSelectedCamIndex(int index) {
        try {
            if (index < 0) {
                index = 0;
            } else if (index >= MAX_CAMERA_COUNT) {
                index = MAX_CAMERA_COUNT - 1;
            }

            int prevIndex = mSelectedCamIndex;
            mSelectedCamIndex = index;

            if (mSelectedCamIndex == 0) {
                mLiveCamRootView1.setSelected(true);

                mLiveCamRootView2.setSelected(false);
                mLiveCamRootView3.setSelected(false);
            } else if (mSelectedCamIndex == 1) {
                mLiveCamRootView2.setSelected(true);

                mLiveCamRootView1.setSelected(false);
                mLiveCamRootView3.setSelected(false);
            } else {
                mLiveCamRootView3.setSelected(true);

                mLiveCamRootView1.setSelected(false);
                mLiveCamRootView2.setSelected(false);
            }

            if (mSelectedCamIndex != prevIndex) {
                mCameraInfoModel = null;
                mOnFragmentInteractionListener.onRequestMqttCameraStatus(mSelectedCamIndex);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startDownloadCamImageTask(final ImageView imageView, final String url) {
        if (imageView.getVisibility() == View.VISIBLE) {
            if (!mIsPaused) {
                new DownloadImageTask(imageView).execute(url);
            }

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startDownloadCamImageTask(imageView, url);
                }
            }, 200);
        }
    }

    private void toggleLiveCamStreaming(int index) {
        switch (index) {
            case 0: {
                if (mCamImageView1.getVisibility() == View.VISIBLE) {
                    mCamImageView1.setVisibility(View.GONE);
                } else {
                    mCamImageView1.setVisibility(View.VISIBLE);
                    startDownloadCamImageTask(mCamImageView1, "http://61.73.59.92:20110/cgi-bin/camera");
                }
            }
            break;
            case 1: {
                if (mCamImageView2.getVisibility() == View.VISIBLE) {
                    mCamImageView2.setVisibility(View.GONE);
                } else {
                    mCamImageView2.setVisibility(View.VISIBLE);
                    startDownloadCamImageTask(mCamImageView2, "http://61.73.59.92:20111/cgi-bin/camera");
                }
            }
            break;
            case 2: {
                if (mCamImageView3.getVisibility() == View.VISIBLE) {
                    mCamImageView3.setVisibility(View.GONE);
                } else {
                    mCamImageView3.setVisibility(View.VISIBLE);
                    startDownloadCamImageTask(mCamImageView3, "http://61.73.59.92:20112/cgi-bin/camera");
                }
            }
            break;
            default:
                break;
        }
    }

    private int getValidCommandValue(Enums.CommandOption commandOption, int value) {
        switch (commandOption) {
            case PAN: {
                return Math.min(Math.max(value, Defines.PAN_MIN_VALUE), Defines.PAN_MAX_VALUE);
            }
            case TILT: {
                return Math.min(Math.max(value, Defines.TILT_MIN_VALUE), Defines.TILT_MAX_VALUE);
            }
            case ZOOM: {
                return Math.min(Math.max(value, Defines.ZOOM_MIN_VALUE), Defines.ZOOM_MAX_VALUE);
            }
            default:
                return value;
        }
    }

    public void setCameraInfoModel(CameraInfoModel model) {
        mCameraInfoModel = model;
    }
}
