package com.cgv.ys.manager.pad.cgv_ys.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by eklee on 2017. 6. 13..
 */

public class UserInfoModel implements Parcelable {
    @SerializedName("MR_ID")
    private String mUserID;

    @SerializedName("MR_NAME")
    private String mUserName;

    @SerializedName("MR_PWD")
    private String mUserPW;

    public UserInfoModel() {
    }

    public UserInfoModel(String userID, String userName, String userPW) {
        mUserID = userID;
        mUserName = userName;
        mUserPW = userPW;
    }

    public String getUserID() {
        return mUserID;
    }

    public void setUserID(String userID) {
        mUserID = userID;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getUserPW() {
        return mUserPW;
    }

    public void setUserPW(String userPW) {
        mUserPW = userPW;
    }

    protected UserInfoModel(Parcel in) {
        mUserID = in.readString();
        mUserName = in.readString();
        mUserPW = in.readString();
    }

    public static final Creator<UserInfoModel> CREATOR = new Creator<UserInfoModel>() {
        @Override
        public UserInfoModel createFromParcel(Parcel in) {
            return new UserInfoModel(in);
        }

        @Override
        public UserInfoModel[] newArray(int size) {
            return new UserInfoModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mUserID);
        dest.writeString(mUserName);
        dest.writeString(mUserPW);
    }
}
