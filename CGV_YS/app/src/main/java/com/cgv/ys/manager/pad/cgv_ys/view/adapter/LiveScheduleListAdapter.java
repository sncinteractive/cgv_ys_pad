package com.cgv.ys.manager.pad.cgv_ys.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cgv.ys.manager.pad.cgv_ys.R;
import com.cgv.ys.manager.pad.cgv_ys.model.LiveScheduleListItemModel;

import java.util.List;

/**
 * Created by eklee on 2017. 6. 16..
 */

public class LiveScheduleListAdapter extends RecyclerView.Adapter<LiveScheduleListAdapter.ViewHolder> {
    public interface ItemInteractionListener {
        void onClick(int position);
    }

    private List<LiveScheduleListItemModel> mModelList;
    private ItemInteractionListener mItemInteractionListener;
    private LiveScheduleListAdapter.ViewHolder.ViewHolderInteractionListener mViewHolderInteractionListener = new ViewHolderInteractionListenerImpl();

    public LiveScheduleListAdapter(List<LiveScheduleListItemModel> modelList, ItemInteractionListener listener) {
        mModelList = modelList;
        mItemInteractionListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_live_schedule_list, parent, false);
        return new ViewHolder(view, mViewHolderInteractionListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            LiveScheduleListItemModel model = mModelList.get(position);
            if (model != null) {
                holder.itemView.setSelected(model.isApplied());
                holder.mTitleTextView.setText(model.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mModelList.size();
    }

    public class ViewHolderInteractionListenerImpl implements LiveScheduleListAdapter.ViewHolder.ViewHolderInteractionListener {

        @Override
        public void onClick(int position) {
            mItemInteractionListener.onClick(position);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public interface ViewHolderInteractionListener {
            void onClick(int position);
        }

        public TextView mTitleTextView;
        private ViewHolderInteractionListener mViewHolderInteractionListener;

        public ViewHolder(View itemView, ViewHolderInteractionListener listener) {
            super(itemView);

            mTitleTextView = (TextView) itemView.findViewById(R.id.tv_title);
            mTitleTextView.setOnClickListener(this);

            mViewHolderInteractionListener = listener;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tv_title: {
                    if (mViewHolderInteractionListener != null) {
                        mViewHolderInteractionListener.onClick(getLayoutPosition());
                    }
                }
                break;
                default:
                    break;
            }
        }
    }
}
